<?php

namespace CA\CaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext; //libreria para el uso de seguridad
use Symfony\Component\HttpFoundation\Request;
use Utilerias\FileMakerBundle\API\ODBC\Client; //ODBC
use Utilerias\FileMakerBundle\API\FM11API; //FM
use Symfony\Component\HttpFoundation\Session\Session; //libreria para el manejo (creacion) de sesiones
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken; ///libreria para generarle un token al usuario
use CA\CaBundle\Model\Profile; //libreria para generar el perfil del usuario

class DefaultController extends Controller
{
	private $ODBC = null;
    private $FM = null;

    public function __construct() //constructor para usar usar al objeto FM y poder utilizar las clases de la libreria ODBC
    {
        $this->ODBC = new Client();
        $this->FM = new FM11API();
    }

	public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR))
        {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } 
        else
        {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
        return $this->render(
            'CACaBundle:Default:index.html.twig',
            array(
                // regresar el ultimo nombre de usuario ingresado
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'error'         => $error,
            )
        );
    }//cierre de la funcion LOGIN

	public function validaAction() //validar a donde enviar al usuario de FM dependiendo de su rol
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        
        //Validamos los datos para Captcha y guardamos en variable $resp
        $resp = recaptcha_check_answer('6Leh8vgSAAAAADFJ2lmxvdMshAUxtvAJZrhBa4qc', $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
        
        //Verificamos el captcha
        if (!$resp->is_valid) {
            $session->getFlashBag()->add('flash1', 'Captcha invalido intente de nuevo');
            return $this->render('CACaBundle:Default:index.html.twig');     
        }

        if ($request->getMethod() == 'POST') 
        {
           $usuario = $request->get('usr');
           $password= $request->get('pwd');
        }

        $pass=sha1($password); //encriptado de contraseña

        $query="select * from USU where US_Usuario='$usuario' and US_Password='$pass' and US_EmailActivado='SI' ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        //print_r();
        //die();
        $privilegios=$result['data'][0]['US_NivelAutoridad'];
           $id=$result['data'][0]['idUsuario'];

        if($privilegios=='ADMIN')
        {
            $mes=intval(date("m/j/Y")); //convertir fecha de formato PHP a FM
            $diayanio=date("/j/Y");
            $fecha=$mes.$diayanio; // 09/10/2014
            //$fecha2=date("m/d/Y"); // 9/10/2014
        	//Creamos el objeto Profile con los datos presentados por el formulario
	        $user = new Profile($result['data'][0]['US_Usuario'], $result['data'][0]['US_Password'], ' ', array('ROLE_ADMIN'));
	        $user->setData($result['data'][0]);
	        // Creamos el token
	        $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());
	        $this->container->get('security.context')->setToken($token);
	        // Creamos e iniciamos la sesión
	        $session->set('_security_main', serialize($token));
            //actualizamos fecha de inicio de sesion
            //$up="update USU set US_UltimaSesion='$fecha' where US_Usuario='$usuario' and US_Password='$pass' ";
            //$this->ODBC->setQuery($up);
            //$this->ODBC->exec();
            $data= null;
            $SParr = null;
            $SParr .= "idUsuario: " . $id . "; ";
            $SParr = str_replace('"', '\"', $SParr);

            $data = $this->FM->Run_Script('PHP_USU', 'PHP_USU_ActualizaFechaSesion', $SParr);
            //redirijimos a la vista
            return $this->render('CACaBundle:Default:admin.html.twig');
        }
        elseif ($privilegios=='USER')
        {
            $mes=intval(date("m/j/Y")); //convertir fecha de formato PHP a FM
            $diayanio=date("/j/Y");
            $fecha=$mes.$diayanio;
            //Creamos el objeto Profile con los datos presentados por el formulario
            $user = new Profile($result['data'][0]['US_Usuario'], $result['data'][0]['US_Password'], ' ', array('ROLE_USER'));
            $user->setData($result['data'][0]);
            // Creamos el token
            $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());
            $this->container->get('security.context')->setToken($token);
            // Creamos e iniciamos la sesión
            $session->set('_security_main', serialize($token));
            //actualizamos fecha de inicio de sesion
            $data= null;
            $SParr = null;
            $SParr .= "idUsuario: " . $id . "; ";
            $SParr = str_replace('"', '\"', $SParr);
            
            $data = $this->FM->Run_Script('PHP_USU', 'PHP_USU_ActualizaFechaSesion', $SParr);
            //redirijimos a la vista
            return $this->render('CACaBundle:Default:user.html.twig');
        }
        elseif ($privilegios=='VIEW')
        {
            $mes=intval(date("m/j/Y")); //convertir fecha de formato PHP a FM
            $diayanio=date("/j/Y");
            $fecha=$mes.$diayanio;
            //Creamos el objeto Profile con los datos presentados por el formulario
            $user = new Profile($result['data'][0]['US_Usuario'], $result['data'][0]['US_Password'], ' ', array('ROLE_VIEW'));
            $user->setData($result['data'][0]);
            // Creamos el token
            $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());
            $this->container->get('security.context')->setToken($token);
            // Creamos e iniciamos la sesión
            $session->set('_security_main', serialize($token));
            //actualizamos fecha de inicio de sesion
            $data= null;
            $SParr = null;
            $SParr .= "idUsuario: " . $id . "; ";
            $SParr = str_replace('"', '\"', $SParr);

            $data = $this->FM->Run_Script('PHP_USU', 'PHP_USU_ActualizaFechaSesion', $SParr);
            //redirijimos a la vista
            return $this->render('CACaBundle:Default:view.html.twig');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Verifique su usuario y contraseña');
            return $this->render('CACaBundle:Default:index.html.twig');
        }
    }//cierre de la funcion validar ROLE y redireccionar

    public function recuperaAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        
        //Validamos los datos para Captcha y guardamos en variable $resp
        $resp = recaptcha_check_answer('6Leh8vgSAAAAADFJ2lmxvdMshAUxtvAJZrhBa4qc', $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
        
        //Verificamos el captcha
        if (!$resp->is_valid) {
            $session->getFlashBag()->add('flash1', 'Captcha invalido intente de nuevo');
            return $this->render('CACaBundle:Default:recupera_view.html.twig');     
        }

        if ($request->getMethod() == 'POST') 
        {
           $correo = $request->get('correo');
        }

        $query="select * from USU where US_Email='$correo' ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();

        if ($result['data'] != '' )
        {
            $passwordRandom = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) .  chr(rand(65,90)) . rand(10, 90); //random 6 caracteres
            $passSHA=sha1($passwordRandom); //encriptado de contraseña nueva

            $update="update USU set US_Password='$passSHA' where US_Email='$correo' ";
            $this->ODBC->setQuery($update);
            $this->ODBC->exec();
            /* Estructura envío de email */
            $content = array(); //content crea un array con datos que se mandaran al twig
            $content['passwordRandom'] = $passwordRandom;
            $content['usuario'] = $result['data'][0]['US_Usuario'];
            $subject='buenpunto.com';

            $body = $this->renderView('CACaBundle:Default:correo.html.twig', array('datos' => $content));
            //parametros: send_email($subject, $to, $body, $lang = 'es')
            $res = $this->get('ixpo_mailer')->send_email($subject, $correo, $body);
            //return $this->render('CACaBundle:Default:correo.html.twig', array('datos' => $content));
            $session->getFlashBag()->add('flash1', 'Se envio un correo a su email');
            return $this->render('CACaBundle:Default:index.html.twig');
            /* Fin estructura envio de Email */
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Por favor verifique su email');
            return $this->render('CACaBundle:Default:recupera_view.html.twig');
        }
         
    }//cierre recupera
    public function cambia_pass_admin2Action()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        //obtener idUsuario
        if ($request->getMethod() == 'POST') 
        {
           $password1 = $request->get('usr');
           $password2= $request->get('pass');
        }
        $passViejo=sha1($password1); //encriptado de contraseña
        $passNuevo=sha1($password2); //encriptado de contraseña

        $data= null;
        $SParr = null;
        $SParr .= "idUsuario: " . $idUsuario . "; ";
        $SParr .= "US_Password: " . $passViejo . "; ";
        $SParr .= "PassNuevo: " . $passNuevo . "; ";
        $SParr = str_replace('"', '\"', $SParr);

        $data = $this->FM->Run_Script('PHP_USU', 'PHP_USU_CambioDeContrasena', $SParr);

        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con exito');
        }    
        return $this->render('CACaBundle:Default:cambiarPass_admin2.html.twig');
    }//cambiar Password

    public function cambia_pass_adminAction()
    {
       return $this->render('CACaBundle:Default:cambiarPass_admin.html.twig');
    }//cambiar Password Vista
    public function cambia_passAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        //obtener idUsuario
        if ($request->getMethod() == 'POST') 
        {
           $password1 = $request->get('usr');
           $password2= $request->get('pass');
        }
        $passViejo=sha1($password1); //encriptado de contraseña
        $passNuevo=sha1($password2); //encriptado de contraseña

        $data= null;
        $SParr = null;
        $SParr .= "idUsuario: " . $idUsuario . "; ";
        $SParr .= "US_Password: " . $passViejo . "; ";
        $SParr .= "PassNuevo: " . $passNuevo . "; ";
        $SParr = str_replace('"', '\"', $SParr);

        $data = $this->FM->Run_Script('PHP_USU', 'PHP_USU_CambioDeContrasena', $SParr);

        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con exito');
        }    
        return $this->render('CACaBundle:Default:cambiarPass_view.html.twig');
    }//cambiar Password

    public function cambia_pass_viewAction()
    {
       return $this->render('CACaBundle:Default:cambiarPass_view2.html.twig');
    }//cambiar Password Vista
    public function cambia_passView_userAction()
    {
       return $this->render('CACaBundle:Default:cambiarPass_user.html.twig');
    }//cambiar Password Vista
    public function recupera_viewAction()
    {
       return $this->render('CACaBundle:Default:recupera_view.html.twig');
    }//mostrat vista de recuperacion
    public function homeAction()
    {
       return $this->render('CACaBundle:Default:perfil.html.twig');
    }//cierre home
    public function logoutAction()
    {
        //debe ir vacia la funcion
    }//cierre logout
    public function mensaje_al_adminAction()
    {
        return $this->render('CACaBundle:Default:mensaje_al_admin.html.twig');
    }//mensajes al administrador
    public function mensaje_al_admin2Action()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        if ($request->getMethod() == 'POST') 
        {
           $asunto = $request->get('asunto');
           $mensaje= $request->get('mensaje');
        }
        $correo="buenpunto@fmdatabases.com";
            $res = $this->get('ixpo_mailer')->send_email($asunto, $correo, $mensaje);
            $session->getFlashBag()->add('flash1', 'Su mensaje fue enviado con exito');
        return $this->redirect($this->generateUrl('user_menu'));
    }//mensajes al administrador
    public function mensaje_al_admin_viewAction()
    {
        return $this->render('CACaBundle:Default:mensaje_al_admin_view.html.twig');
    }//mensajes al administrador
    public function mensaje_al_admin2_viewAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        if ($request->getMethod() == 'POST') 
        {
           $asunto = $request->get('asunto');
           $mensaje= $request->get('mensaje');
        }
        $correo="buenpunto@fmdatabases.com";
            $res = $this->get('ixpo_mailer')->send_email($asunto, $correo, $mensaje);
            $session->getFlashBag()->add('flash1', 'Su mensaje fue enviado con exito');
        return $this->redirect($this->generateUrl('view_menu'));
    }//mensajes al administrador
    public function cambia_pass2Action()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        //obtener idUsuario
        if ($request->getMethod() == 'POST') 
        {
           $password1 = $request->get('usr');
           $password2= $request->get('pass');
        }
        $passViejo=sha1($password1); //encriptado de contraseña
        $passNuevo=sha1($password2); //encriptado de contraseña

        $data= null;
        $SParr = null;
        $SParr .= "idUsuario: " . $idUsuario . "; ";
        $SParr .= "US_Password: " . $passViejo . "; ";
        $SParr .= "PassNuevo: " . $passNuevo . "; ";
        $SParr = str_replace('"', '\"', $SParr);

        $data = $this->FM->Run_Script('PHP_USU', 'PHP_USU_CambioDeContrasena', $SParr);

        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con exito');
        }    
        return $this->render('CACaBundle:Default:cambiarPass_view2.html.twig');
    }//cambiar Password
    public function cambia_pass_userAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        //obtener idUsuario
        if ($request->getMethod() == 'POST') 
        {
           $password1 = $request->get('usr');
           $password2= $request->get('pass');
        }
        $passViejo=sha1($password1); //encriptado de contraseña
        $passNuevo=sha1($password2); //encriptado de contraseña

        $data= null;
        $SParr = null;
        $SParr .= "idUsuario: " . $idUsuario . "; ";
        $SParr .= "US_Password: " . $passViejo . "; ";
        $SParr .= "PassNuevo: " . $passNuevo . "; ";
        $SParr = str_replace('"', '\"', $SParr);

        $data = $this->FM->Run_Script('PHP_USU', 'PHP_USU_CambioDeContrasena', $SParr);

        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con exito');
        }    
        return $this->render('CACaBundle:Default:cambiarPass_user2.html.twig');
    }//cambiar Password

    //MENUS
    public function admin_menuAction()
    {
       return $this->render('CACaBundle:Default:admin.html.twig');
    }
    public function user_menuAction()
    {
       return $this->render('CACaBundle:Default:user.html.twig');
    }
    public function view_menuAction()
    {
       return $this->render('CACaBundle:Default:view.html.twig');
    }//cierre MENUS
}

//--------------------COMIENZO DEL CODIGO CAPTCHA NO MODIFICAR ------------------------------------------------------------

/**
 * The reCAPTCHA server URL's
 */
define("RECAPTCHA_API_SERVER", "http://www.google.com/recaptcha/api");
define("RECAPTCHA_API_SECURE_SERVER", "https://www.google.com/recaptcha/api");
define("RECAPTCHA_VERIFY_SERVER", "www.google.com");

/**
 * Encodes the given data into a query string format
 * @param $data - array of string elements to be encoded
 * @return string - encoded request
 */
function _recaptcha_qsencode($data) {
    $req = "";
    foreach ($data as $key => $value)
        $req .= $key . '=' . urlencode(stripslashes($value)) . '&';

    // Cut the last '&'
    $req = substr($req, 0, strlen($req) - 1);
    return $req;
}

/**
 * Submits an HTTP POST to a reCAPTCHA server
 * @param string $host
 * @param string $path
 * @param array $data
 * @param int port
 * @return array response
 */
function _recaptcha_http_post($host, $path, $data, $port = 80) {

    $req = _recaptcha_qsencode($data);

    $http_request = "POST $path HTTP/1.0\r\n";
    $http_request .= "Host: $host\r\n";
    $http_request .= "Content-Type: application/x-www-form-urlencoded;\r\n";
    $http_request .= "Content-Length: " . strlen($req) . "\r\n";
    $http_request .= "User-Agent: reCAPTCHA/PHP\r\n";
    $http_request .= "\r\n";
    $http_request .= $req;

    $response = '';
    if (false == ( $fs = @fsockopen($host, $port, $errno, $errstr, 10) )) {
        die('Could not open socket');
    }

    fwrite($fs, $http_request);

    while (!feof($fs))
        $response .= fgets($fs, 1160); // One TCP-IP packet
    fclose($fs);
    $response = explode("\r\n\r\n", $response, 2);

    return $response;
}

/**
 * Gets the challenge HTML (javascript and non-javascript version).
 * This is called from the browser, and the resulting reCAPTCHA HTML widget
 * is embedded within the HTML form it was called from.
 * @param string $pubkey A public key for reCAPTCHA
 * @param string $error The error given by reCAPTCHA (optional, default is null)
 * @param boolean $use_ssl Should the request be made over ssl? (optional, default is false)

 * @return string - The HTML to be embedded in the user's form.
 */
function recaptcha_get_html($pubkey, $error = null, $use_ssl = false) {
    if ($pubkey == null || $pubkey == '') {
        die("To use reCAPTCHA you must get an API key from <a href='https://www.google.com/recaptcha/admin/create'>https://www.google.com/recaptcha/admin/create</a>");
    }

    if ($use_ssl) {
        $server = RECAPTCHA_API_SECURE_SERVER;
    } else {
        $server = RECAPTCHA_API_SERVER;
    }

    $errorpart = "";
    if ($error) {
        $errorpart = "&amp;error=" . $error;
    }
    return '<script type="text/javascript" src="' . $server . '/challenge?k=' . $pubkey . $errorpart . '"></script>

        <noscript>
                <iframe src="' . $server . '/noscript?k=' . $pubkey . $errorpart . '" height="300" width="500" frameborder="0"></iframe><br/>
                <textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
                <input type="hidden" name="recaptcha_response_field" value="manual_challenge"/>
        </noscript>';
}

/**
 * A ReCaptchaResponse is returned from recaptcha_check_answer()
 */
class ReCaptchaResponse {

    var $is_valid;
    var $error;

}

/**
 * Calls an HTTP POST function to verify if the user's guess was correct
 * @param string $privkey
 * @param string $remoteip
 * @param string $challenge
 * @param string $response
 * @param array $extra_params an array of extra variables to post to the server
 * @return ReCaptchaResponse
 */
function recaptcha_check_answer($privkey, $remoteip, $challenge, $response, $extra_params = array()) {
    if ($privkey == null || $privkey == '') {
        die("To use reCAPTCHA you must get an API key from <a href='https://www.google.com/recaptcha/admin/create'>https://www.google.com/recaptcha/admin/create</a>");
    }

    if ($remoteip == null || $remoteip == '') {
        die("For security reasons, you must pass the remote ip to reCAPTCHA");
    }



    //discard spam submissions
    if ($challenge == null || strlen($challenge) == 0 || $response == null || strlen($response) == 0) {
        $recaptcha_response = new ReCaptchaResponse();
        $recaptcha_response->is_valid = false;
        $recaptcha_response->error = 'incorrect-captcha-sol';
        return $recaptcha_response;
    }

    $response = _recaptcha_http_post(RECAPTCHA_VERIFY_SERVER, "/recaptcha/api/verify", array(
        'privatekey' => $privkey,
        'remoteip' => $remoteip,
        'challenge' => $challenge,
        'response' => $response
            ) + $extra_params
    );

    $answers = explode("\n", $response [1]);
    $recaptcha_response = new ReCaptchaResponse();

    if (trim($answers [0]) == 'true') {
        $recaptcha_response->is_valid = true;
    } else {
        $recaptcha_response->is_valid = false;
        $recaptcha_response->error = $answers [1];
    }
    return $recaptcha_response;
}

/**
 * gets a URL where the user can sign up for reCAPTCHA. If your application
 * has a configuration page where you enter a key, you should provide a link
 * using this function.
 * @param string $domain The domain where the page is hosted
 * @param string $appname The name of your application
 */
function recaptcha_get_signup_url($domain = null, $appname = null) {
    return "https://www.google.com/recaptcha/admin/create?" . _recaptcha_qsencode(array('domains' => $domain, 'app' => $appname));
}

function _recaptcha_aes_pad($val) {
    $block_size = 16;
    $numpad = $block_size - (strlen($val) % $block_size);
    return str_pad($val, strlen($val) + $numpad, chr($numpad));
}

/* Mailhide related code */

function _recaptcha_aes_encrypt($val, $ky) {
    if (!function_exists("mcrypt_encrypt")) {
        die("To use reCAPTCHA Mailhide, you need to have the mcrypt php module installed.");
    }
    $mode = MCRYPT_MODE_CBC;
    $enc = MCRYPT_RIJNDAEL_128;
    $val = _recaptcha_aes_pad($val);
    return mcrypt_encrypt($enc, $ky, $val, $mode, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0");
}

function _recaptcha_mailhide_urlbase64($x) {
    return strtr(base64_encode($x), '+/', '-_');
}

/* gets the reCAPTCHA Mailhide url for a given email, public key and private key */

function recaptcha_mailhide_url($pubkey, $privkey, $email) {
    if ($pubkey == '' || $pubkey == null || $privkey == "" || $privkey == null) {
        die("To use reCAPTCHA Mailhide, you have to sign up for a public and private key, " .
                "you can do so at <a href='http://www.google.com/recaptcha/mailhide/apikey'>http://www.google.com/recaptcha/mailhide/apikey</a>");
    }


    $ky = pack('H*', $privkey);
    $cryptmail = _recaptcha_aes_encrypt($email, $ky);

    return "http://www.google.com/recaptcha/mailhide/d?k=" . $pubkey . "&c=" . _recaptcha_mailhide_urlbase64($cryptmail);
}

/**
 * gets the parts of the email to expose to the user.
 * eg, given johndoe@example,com return ["john", "example.com"].
 * the email is then displayed as john...@example.com
 */
function _recaptcha_mailhide_email_parts($email) {
    $arr = preg_split("/@/", $email);

    if (strlen($arr[0]) <= 4) {
        $arr[0] = substr($arr[0], 0, 1);
    } else if (strlen($arr[0]) <= 6) {
        $arr[0] = substr($arr[0], 0, 3);
    } else {
        $arr[0] = substr($arr[0], 0, 4);
    }
    return $arr;
}

/**
 * Gets html to display an email address given a public an private key.
 * to get a key, go to:
 *
 * http://www.google.com/recaptcha/mailhide/apikey
 */
function recaptcha_mailhide_html($pubkey, $privkey, $email) {
    $emailparts = _recaptcha_mailhide_email_parts($email);
    $url = recaptcha_mailhide_url($pubkey, $privkey, $email);

    return htmlentities($emailparts[0]) . "<a href='" . htmlentities($url) .
            "' onclick=\"window.open('" . htmlentities($url) . "', '', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=500,height=300'); return false;\" title=\"Reveal this e-mail address\">...</a>@" . htmlentities($emailparts [1]);
}

//-------------------------------TERMINO DE CODIGO CAPTCHA -----------------------------------------------------------------------------