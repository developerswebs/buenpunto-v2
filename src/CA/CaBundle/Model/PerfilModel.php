<?php

namespace CA\CaBundle\Model;

use Utilerias\FileMakerBundle\API\ODBC\Client;

class PerfilModel {

    protected $FM, $FM_Client, $Layout = 'GE_Visitante';

    public function __construct() {
        $this->FM_Client = new Client();
    }

    public function getVisitante($Args, $AllData = FALSE) {
        $qry = 'SELECT ' . $this->getCamposVisitante() . ' FROM Visitante' . $this->buildCondition($Args);
        $this->FM_Client->setQuery($qry);
        $this->FM_Client->exec();
        $result = $this->FM_Client->getResultAssoc();
        if ($result['status']) {
            if (COUNT($result['data']) > 0 && $AllData) {
                foreach ($result['data'] as $key => $value) {
                    $Args = Array('"_id_Visitante"' => $value['_id_Visitante']);
                    $visitante_edicion_result = $this->getStatusVisitanteEdicion($Args);
                    if (!$visitante_edicion_result['status']) {
                        return $visitante_edicion_result;
                    }
                    $result['data'][$key]['Portal_GE_Visitante_Edicion'] = $visitante_edicion_result['data'];
                }
            }
        }
        return $result;
    }

    private function getCamposVisitante() {
        $fields = '';
        $fields .= '"_id_Visitante",';
        //$fields .= '"_id_Visitante_Padre",';
        $fields .= 'Nombre,';
        $fields .= 'ApellidoPaterno,';
        $fields .= 'ApellidoMaterno,';
        $fields .= 'Email,';
        $fields .= 'AreaPais,';
        $fields .= 'AreaCiudad,';
        $fields .= 'Telefono,';
        $fields .= 'TelExt,';
        $fields .= 'Password,';
        $fields .= '"_id_Visitante_Tipo",';
        $fields .= 'FechaNacimiento,';
        $fields .= 'Movil,';
        $fields .= 'Nextel,';
        $fields .= 'Fax,';
        $fields .= 'Sexo,';
        $fields .= 'Titulo,';
        $fields .= '"DE_Razon_Social",';
        $fields .= '"DE_Nombre",';
        $fields .= '"DE_Cargo",';
        $fields .= '"DE_Pais",';
        $fields .= '"DE_id_Pais",';
        $fields .= '"DE_CP",';
        $fields .= '"DE_Estado",';
        $fields .= '"DE_id_Estado",';
        $fields .= '"DE_Colonia",';
        $fields .= '"DE_id_Colonia",';
        $fields .= '"DE_Ciudad",';
        $fields .= '"DE_id_Ciudad",';
        $fields .= '"DE_Direccion",';
        $fields .= '"DE_Calle",';
        $fields .= '"DE_NumeroExterior",';
        $fields .= '"DE_NumeroInterior",';
        $fields .= '"DE_AreaPais",';
        $fields .= '"DE_AreaCiudad",';
        $fields .= '"DE_Telefono",';
        $fields .= '"DE_ExtTel",';
        $fields .= '"DE_Email",';
        $fields .= '"DE_WebPage",';
        $fields .= '"DE_RFC",';
        $fields .= 'Boletines,';
        $fields .= '"Email_Opcional",';
        $fields .= 'ScoreEncuesta,';
        $fields .= 'TokenPassword';
        return $fields;
    }

    private function getStatusVisitanteEdicion($Args) {
        $qry = 'SELECT "_id_Edicion", EncuestaCompleta, Pregistrado FROM "Visitante_Edicion" ' . $this->buildCondition($Args) . '';
        $this->FM_Client->setQuery($qry);
        $this->FM_Client->exec();
        return $this->FM_Client->getResultAssoc();
    }

    public function updateVisitor($Values, $Args) {
        $field_update = "";
        $i = 1;
        foreach ($Values as $key => $value) {
            $field_update.= $key;
            $field_update.= ($value == "") ? " = '' " : " = " . $value;
            if ($i < COUNT($Values)) {
                $field_update.= ', ';
            }
            $i++;
        }
        $qry = 'UPDATE Visitante SET ' . $field_update . ' ' . $this->buildCondition($Args) . '';
        $this->FM_Client->setQuery($qry);
        $this->FM_Client->exec();
        return $this->FM_Client->getResultAssoc();
    }

    /**
     * 
     * @param type $Fields Arreglo de campos que deseas obtener de la tabla
     * @param type $tabla Nombre de la tabla
     * @param type $Args Arreglo de los argumentos para la condicion de la consulta [WHERE]
     * @return type
     */
    public function select_from_table($Fields, $tabla, $Args, $OrderBy = NULL) {
        $qry = 'SELECT ' . $this->buildStringtFields($Fields) . ' FROM ' . $tabla . ' ' . $this->buildCondition($Args) . ' ' . $this->buildOrderBy($OrderBy);

        $this->FM_Client->setQuery($qry);
        $res = $this->FM_Client->exec();
        return $this->FM_Client->getResultAssoc();
    }

    /**
     * 
     * @param type $Fields Arreglo de campos que deseas obtener de la tabla
     * @param type $tabla Nombre de la tabla
     * @param type $Args Arreglo de los argumentos para la condicion de la consulta [WHERE]
     * @return type
     */
    public function delete_from_table($Args, $tabla) {
        $this->FM_Client->setCache(0);
        $qry = 'DELETE FROM ' . $tabla . ' ' . $this->buildCondition($Args);
        $this->FM_Client->setQuery($qry);

        $res = $this->FM_Client->exec();
        return $this->FM_Client->getResultAssoc();
    }

    /**
     * 
     * @param type $Args Arreglo de valores que se insertarán en la tabla array('nombre_de_campo' => 'valor')
     * @param type $table nombre de la Tabla
     */
    public function insert_into_table($Args, $table) {
        if (count($Args) > 0) {
            $i = 1;
            $column = "( ";
            $column_value = "( ";
            foreach ($Args as $key => $value) {
                $column .= $key;
                $column_value .= ($value == '') ? 'NULL' : $value;

                if ($i < COUNT($Args)) {
                    $column.= ', ';
                    $column_value.= ', ';
                }
                $i++;
            }
            $column .= " )";
            $column_value .= " )";

            $qry = 'INSERT INTO ' . $table;
            $qry .= ' ' . $column;
            $qry .= ' VALUES ';
            $qry .= ' ' . $column_value;
            $this->FM_Client->setQuery($qry);
            $res = $this->FM_Client->exec();
            return $this->FM_Client->getResultAssoc();
        }
        return array('status' => FALSE, 'data' => 'Sin campos a insertar');
    }

    /**
     * 
     * @param type $Values Arreglo de los valores para actualuzar [SET]
     * @param type $Args Arreglo de los argumentos para la condicion de la consulta [WHERE]
     * @param type $table Nombre de la tabla para actualizar
     */
    public function update_table($Values, $Args, $table) {
        $field_update = "";
        $i = 1;
        foreach ($Values as $key => $value) {
            $field_update.= $key;
            $field_update.= ($value == "") ? " = '' " : " = " . $value;
            if ($i < COUNT($Values)) {
                $field_update.= ', ';
            }
            $i++;
        }
        $qry = 'UPDATE ' . $table . ' SET ' . $field_update . ' ' . $this->buildCondition($Args) . '';
        $this->FM_Client->setQuery($qry);
        $res = $this->FM_Client->exec();
        return $this->FM_Client->getResultAssoc();
    }

    public function buildCondition($Args) {
        $condicion = '';

        if (is_array($Args)) {
            $condicion .= ' WHERE ';
        } else {
            return '';
        }
        $i = 1;
        foreach ($Args as $key => $value) {
            $condicion.= $key;
            if (substr($value, 0, 1) == "'" && substr($value, -1) == "'") {
                $value = "'" . $this->formatQuery(substr($value, 1, -1)) . "'";
            }
            $condicion.= ($value == "") ? " IS NULL " : "=" . $value;
            if ($i < COUNT($Args)) {
                $condicion.= ' AND ';
            }
            $i++;
        }
        return $condicion;
    }

    /**
      Replace characters with accentos and eñes por su código correspondiente en filemaker
     */
    private function formatQuery($str) {
        $patterns = array("'");
        $replacements = array("'+CHR(39)+'");
        $str = str_replace($patterns, $replacements, $str);
        return $str;
    }

    /**
     * 
     * @param type Array $Fields Arreglo de campos que deseas obtener de la tabla
     * @return string
     */
    public function buildStringtFields($Fields) {
        $stringFields = '';
        $i = 1;

        if (count($Fields) > 0) {
            foreach ($Fields as $value) {
                $stringFields.= $value;
                if ($i < COUNT($Fields)) {
                    $stringFields.= ', ';
                }
                $i++;
            }
        } else {
            $stringFields.= '* ';
        }
        return $stringFields;
    }

    public function buildOrderBy($Args) {
        $stringFields = '';

        if (is_array($Args)) {
            $stringFields .= ' ORDER BY ';
        } else {
            return '';
        }
        $i = 1;
        foreach ($Args as $key => $value) {
            $stringFields.= $key . ' ' . $value;
            if ($i < COUNT($Args)) {
                $stringFields.= ', ';
            }
            $i++;
        }
        return $stringFields;
    }

    /**
     * Formula para generar la contraseña: Ciclo de longitud 9. aleatoriamente escoge letra o numero, por ultimo los reordena
     * @param type $user Arreglo de los datos del visitante
     * @return type String Password generado
     */
    public function generatePassword() {
        $pass = '';
        for ($i = 0; $i < 9; $i++){
            $pass .= (rand(0, 1))?chr(rand(65, 90)):chr(rand(48, 57));
        }
        return str_shuffle($pass);
    }

    /**
     * Formula para generar la contraseña: Ultima letra del apellido Paterno +  idVisitante + Primera letra del Nombre + Una letra Aleatoria.
     * @param type $user Arreglo de los datos del visitante
     * @return type String Password generado
     */
    public function generatePasswordUser($user) {
        $pre_pass = substr($user['ApellidoPaterno'], -1) . $this->completeID($user['_id_Visitante']) . substr($user['Nombre'], 0, 1) . chr(rand(65, 90));
        $pre_pass = str_shuffle($pre_pass);
        return $pre_pass;
    }

    public function isValidEmail($email) {
        $pattern = "/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+/";
        if (preg_match($pattern, strtolower($email)))
            return true;
        return false;
    }

    public function createString($ar, $post) {
        $SParr = "_ar: $ar; ";
        foreach ($post as $key => $value) {
            $value = trim($value);
            $SParr .= "$key: $value; ";
        }
        return $SParr;
    }

    public function completeID($id, $length = 6) {
        $i = strlen($id);
        $compl = '';
        for (; $i < $length; $i++) {
            $compl .= '0';
        }
        return $compl . $id;
    }

    public function writeJSON($filename, $array) {
        $json = json_encode($array);
        $fp = fopen($filename, "w");
        fwrite($fp, $json);
        fclose($fp);
    }

}
