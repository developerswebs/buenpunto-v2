<?php

namespace CA\CaBundle\Twig;

/**
 * Description of UserAgent
 *
 * @author Javier
 */
class UserAgent extends \Twig_Extension {

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('browser', array($this, 'browserDetectFilter')),
        );
    }

    public function browserDetectFilter($userAgent) {
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match("/Explorer/", $userAgent) || $this->getInternetExplorerVersion())
            return 6;
        else if (preg_match("/iPad/", $userAgent))
            return 1;
        else if (preg_match("/iPhone/", $userAgent) || preg_match("/iPod/", $userAgent))
            return 2;
        else if (preg_match("/Firefox/", $userAgent))
            return 3;
        else if (preg_match("/Chrome/", $userAgent))
            return 4;
        else if (preg_match("/Safari/", $userAgent))
            return 5;
        else if (preg_match("/Android/", $userAgent))
            return 7;
        return 0;
    }

    private function getInternetExplorerVersion() {

        $userAgent = $_SERVER['HTTP_USER_AGENT'];

        preg_match("/MSIE ([0-9]{1,}[\.0-9]{0,})/", $userAgent, $matches, PREG_OFFSET_CAPTURE);

        if (isset($matches[0][0]) && $matches[0][0] != "")
            return $matches[0][0];
        else
            return 0;
    }
    
    public function getName() {
        return 'user_agent';
    }

}