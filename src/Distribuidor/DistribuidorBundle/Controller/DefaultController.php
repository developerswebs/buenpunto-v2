<?php

namespace Distribuidor\DistribuidorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Utilerias\FileMakerBundle\API\ODBC\Client; //libreria para conexion con ODBC, para querys
use Utilerias\FileMakerBundle\API\FM11API; //libreria para conexion a FM, para insert, delete, update

class DefaultController extends Controller
{
    private $ODBC = null;
    private $FM = null;

    public function __construct() { //constructor para usar usar al objeto FM y poder utilizar las clases de la libreria ODBC
        $this->ODBC = new Client();
        $this->FM = new FM11API();
    } //CONSTRUCTORES PARA LIBRERIAS

    public function distribuidorAction()
    {
    	$query="select * from DIST order by idDistribuidor";
    	$this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        return $this->render('DistribuidorDistribuidorBundle:Default:distribuidor.html.twig', array('distribuidores' => $result));
    }
    public function agregar_distribuidorAction()
    {
        //arreglo pasa tipos de sociedades
        $tipoSoc['tipoSoc']=array('Persona Física','A.C.','CO.','CO. LTD','GMBH','S.A.','S.A. DE C.V.','S.A.P.I. DE C.V.','S. DE R.L. DE C.V.','S. DE R.L. MI','S.C.','S.C.L.','S.P.A.','S.L.','L.T.D.','LTDA','L.L.C.','S.R.L.','INC.');
        //arreglo pasa tipos de sociedades
        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');

        return $this->render('DistribuidorDistribuidorBundle:Default:agregar_distribuidor.html.twig', array('tipoSoc' => $tipoSoc, 'paises' => $result_paises ));
    }//mostrar pagina de agregar
    public function addDistribuidorAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
    	$user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        //obtener idUsuario
    	$request = $this->getRequest();
    	if ($request->getMethod() == 'POST')
    	{
            $id = $request->get('idDistribuidor');
            $nom = $this->limpiar($request->get('inputNom'));//manteiene solo los caracteres alfanumericos
            $rfc = $this->limpiar($request->get('inputRFC'));
            $calle = $this->limpiar($request->get('calle'));
            $col = $this->limpiar($request->get('col'));
            $pais = $this->limpiar($request->get('DE_id_Pais'));
            $estado = $this->limpiar($request->get('DE_id_Estado'));
            $ciudad = $this->limpiar($request->get('DE_id_Ciudad'));
            $cp = $this->limpiar($request->get('inputCP'));
            $rs = $this->limpiar($request->get('rs'));
            $trs = $this->limpiar($request->get('trs'));
    	}
        if($ciudad == '')
        {
            $ciudad="0";
        }
        $data= null;
        $SParr = null;
        $SParr .= "idDistribuidor: " . $id . "; ";
        $SParr .= "DI_Nombre: " . $nom . "; ";
        $SParr .= "DI_RFC: " . $rfc . "; ";
        $SParr .= "DI_Calle: " . $calle . "; ";
        $SParr .= "DI_Colonia: " . $col . "; ";
        $SParr .= "_id_Pais: " . $pais . "; ";
        $SParr .= "_id_Ciudad: " . $ciudad . "; ";
        $SParr .= "_id_Estado: " . $estado . "; ";
        $SParr .= "DI_CP: " . $cp . "; ";
        $SParr .= "DI_RazonSocial: " . $rs . "; ";
        $SParr .= "DI_TipoSoc: " . $trs . "; ";
        $SParr .= "idUsuarioCreacion: " . $idUsuario . "; ";
        $SParr = str_replace('"', '\"', $SParr);

        $data = $this->FM->Run_Script('PHP_DIST', 'PHP_DIST_Insert_Update', $SParr);
        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
        }    
        return $this->redirect($this->generateUrl('distribuidor'));
    }//agregar distribuidor

    public function detalle_distribuidorAction($idDistribuidor)
    {
        $request = $this->getRequest();
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idDistribuidor');
        }
        $query="select * from DIST where idDistribuidor=$id ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();

        $query2="select * from USU where idDistribuidor=$id ";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $result2 = $this->ODBC->getResultAssoc();

        return $this->render('DistribuidorDistribuidorBundle:Default:detalle_distribuidor.html.twig', array('distribuidor' => $result, 'usuario' => $result2 ));
    }//mostrar detalle_distribuidor
    public function editar_distribuidorAction($idDistribuidor)
    {
        $request = $this->getRequest();
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idDistribuidor');
        }
        $query="select * from DIST where idDistribuidor=$id ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        //arreglo pasa tipos de sociedades
        $tipoSoc['tipoSoc']=array('Persona Física','A.C.','CO.','CO. LTD','GMBH','S.A.','S.A. DE C.V.','S.A.P.I. DE C.V.','S. DE R.L. DE C.V.','S. DE R.L. MI','S.C.','S.C.L.','S.P.A.','S.L.','L.T.D.','LTDA','L.L.C.','S.R.L.','INC.');
        //arreglo pasa tipos de sociedades
        $idPais=$result['data'][0]['_id_Pais'];

        if ($idPais == '')
        {
        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');
        return $this->render('DistribuidorDistribuidorBundle:Default:editar_distribuidor.html.twig', array('distribuidor' => $result, 'tipoSoc' => $tipoSoc, 'paises' => $result_paises ));
        }
        else
        {
        $idEstado=$result['data'][0]['_id_Estado'];
        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');
        $reult_estados = $PECC->getEstados($idPais);
        $reult_ciudades = $PECC->getCiudades($idEstado);

        return $this->render('DistribuidorDistribuidorBundle:Default:editar_distribuidor.html.twig', array('distribuidor' => $result, 'tipoSoc' => $tipoSoc, 'paises' => $result_paises, 'estados' => $reult_estados, 'ciudades' => $reult_ciudades ));
        }
    }//mostrar editar_distribuidor
    public function limpiar($msj)
    {
        $msj = str_replace(
        array("\\", "¨", "º", "-", "~",
        "#", "|", "!", "\"",
        "·", "$", "%", "&", "/",
        "(", ")", "?", "'", "¡",
        "¿", "[", "^", "`", "]",
        "+", "}", "{", "¨", "´",
        ">", "< ", ";", ",", ":",
        "."),
        '',
        $msj
        );
        return $msj;
    }
}//cierre de clase