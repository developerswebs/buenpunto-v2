<?php

namespace Movimiento\MovimientoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Utilerias\FileMakerBundle\API\ODBC\Client; //libreria para conexion con ODBC, para querys
use Utilerias\FileMakerBundle\API\FM11API; //libreria para conexion a FM, para insert, delete, update

class DefaultController extends Controller
{
    private $ODBC = null;
    private $FM = null;

    public function __construct() { //constructor para usar usar al objeto FM y poder utilizar las clases de la libreria ODBC
        $this->ODBC = new Client();
        $this->FM = new FM11API();
    } //CONSTRUCTORES PARA LIBRERIAS

    public function movimientoAction()
    {
    	$query="select idCliente,CL_Nombre from CTE order by idCliente";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $clientes = $this->ODBC->getResultAssoc();
        return $this->render('MovimientoMovimientoBundle:Default:movimiento.html.twig', array('clientes' => $clientes ));
    }
    public function agregar_movimientoAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        $idDistribuidor=$perfil['idDistribuidor'];
        //obtener idUsuario
        $request = $this->getRequest();
        if ($request->getMethod() == 'POST')
        {
            $idCliente = $request->get('cliente');
            $idSituacion = $request->get('situacion');
            $idTipo = $request->get('tipo');
            $monto = $request->get('monto');
            $accion = $request->get('accion');
            $idReporte = $request->get('idReporte');
        }
        if ($monto == '')
        {
            $monto=0;
        }
        $data= null;
        $SParr = null;
        $SParr .= "idCliente: " . $idCliente . "; ";
        $SParr .= "idDistribuidor: " . $idDistribuidor . "; ";
        $SParr .= "idUsuario: " . $idUsuario . "; ";
        $SParr .= "idSituacion: " . $idSituacion . "; ";
        $SParr .= "idTipo: " . $idTipo . "; ";
        $SParr .= "idReporte: " . $idReporte . "; ";
        $SParr .= "RP_Rango: " . $monto . "; "; //monto
        $SParr .= "accion: " . $accion . "; "; //accion 1 insert normal 2 insert de tipo update
        $SParr = str_replace('"', '\"', $SParr);
        //print_r($SParr);
        //die();
        $data = $this->FM->Run_Script('PHP_RPT', 'PHP_RPT_Insert_Mov', $SParr);
        return $this->redirect($this->generateUrl('ver_movimientos',array('idCliente' => $idCliente)));
    }//agregar movimiento
    public function agregar_movimiento_userAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        $idDistribuidor=$perfil['idDistribuidor'];
        //obtener idUsuario
        $request = $this->getRequest();
        if ($request->getMethod() == 'POST')
        {
            $idCliente = $request->get('cliente');
            $idSituacion = $request->get('situacion');
            $idTipo = $request->get('tipo');
            $monto = $request->get('monto');
            $accion = $request->get('accion');
            $idReporte = $request->get('idReporte');
        }
        if ($monto == '')
        {
            $monto=0;
        }
        $data= null;
        $SParr = null;
        $SParr .= "idCliente: " . $idCliente . "; ";
        $SParr .= "idDistribuidor: " . $idDistribuidor . "; ";
        $SParr .= "idUsuario: " . $idUsuario . "; ";
        $SParr .= "idSituacion: " . $idSituacion . "; ";
        $SParr .= "idTipo: " . $idTipo . "; ";
        $SParr .= "idReporte: " . $idReporte . "; ";
        $SParr .= "RP_Rango: " . $monto . "; "; //monto
        $SParr .= "accion: " . $accion . "; "; //accion 1 insert normal 2 insert de tipo update
        $SParr = str_replace('"', '\"', $SParr);
        //print_r($SParr);
        //die();
        $data = $this->FM->Run_Script('PHP_RPT', 'PHP_RPT_Insert_Mov', $SParr);
        return $this->redirect($this->generateUrl('ver_movimientos_user',array('idCliente' => $idCliente)));
    }//agregar movimiento
    public function ver_movimientosAction($idCliente)
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];

        $query="select * from CTE where idCliente=$idCliente ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $cliente = $this->ODBC->getResultAssoc();

        $query2="select a.idUsuario,a.c_RP_Numero,a.RP_FechaCreacion,a.idSituacion,a.idTipo,a.RP_Rango,b.SI_Clave,c.TI_Clave,a.idReporte,a.RP_Balance from RPT a inner join SIT b on(a.idSituacion=b.idSituacion) inner join TIP c on(a.idTipo=c.idTipo) where a.idCliente=$idCliente order by a.c_RP_Numero ";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $detalle = $this->ODBC->getResultAssoc();

        $temp = array();
        foreach ($detalle['data'] as $value) {
            if(!isset($temp[$value['c_RP_Numero']])){
            $temp[$value['c_RP_Numero']] = array();
            }
            array_push($temp[$value['c_RP_Numero']],$value);
        }
        return $this->render('MovimientoMovimientoBundle:Default:ver_movimientos.html.twig', array('cliente' => $cliente, 'detalle' => $temp, 'idUsuario' => $idUsuario, 'idCliente' => $idCliente));
    }//ver movimientos de los clientes
    public function ver_movimientos_userAction($idCliente)
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];

        $query="select * from CTE where idCliente=$idCliente ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $cliente = $this->ODBC->getResultAssoc();

        $query2="select a.idUsuario,a.c_RP_Numero,a.RP_FechaCreacion,a.idSituacion,a.idTipo,a.RP_Rango,b.SI_Clave,c.TI_Clave,a.idReporte,a.RP_Balance from RPT a inner join SIT b on(a.idSituacion=b.idSituacion) inner join TIP c on(a.idTipo=c.idTipo) where a.idCliente=$idCliente order by a.c_RP_Numero ";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $detalle = $this->ODBC->getResultAssoc();

        $temp = array();
        foreach ($detalle['data'] as $value) {
            if(!isset($temp[$value['c_RP_Numero']])){
            $temp[$value['c_RP_Numero']] = array();
            }
            array_push($temp[$value['c_RP_Numero']],$value);
        }

        return $this->render('MovimientoMovimientoBundle:Default:ver_movimientos_user.html.twig', array('cliente' => $cliente, 'detalle' => $temp, 'idUsuario' => $idUsuario, 'idCliente' => $idCliente));
    }//ver movimientos de los clientes
    public function movimiento2Action($idReporte,$idCliente,$idTipo)
    {
        $query="select * from CTE where idCliente=$idCliente ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $cliente = $this->ODBC->getResultAssoc();

        return $this->render('MovimientoMovimientoBundle:Default:movimiento2.html.twig', array('cliente' => $cliente, 'idCliente' => $idCliente, 'idReporte' => $idReporte, 'idTipo' => $idTipo ));
    }//actualizar movimiento - realmente es insert
    public function movimiento2_userAction($idReporte,$idCliente,$idTipo)
    {
        $query="select * from CTE where idCliente=$idCliente ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $cliente = $this->ODBC->getResultAssoc();

        return $this->render('MovimientoMovimientoBundle:Default:movimiento2_user.html.twig', array('cliente' => $cliente, 'idCliente' => $idCliente, 'idReporte' => $idReporte, 'idTipo' => $idTipo ));
    }//actualizar movimiento - realmente es insert


    //USER
    public function movimiento_userAction()
    {
        $query="select idCliente,CL_Nombre from CTE order by idCliente";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $clientes = $this->ODBC->getResultAssoc();
        return $this->render('MovimientoMovimientoBundle:Default:movimiento_user.html.twig', array('clientes' => $clientes ));
    }
     public function ver_movimientos_viewAction($idCliente)
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];

        $query="select * from CTE where idCliente=$idCliente ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $cliente = $this->ODBC->getResultAssoc();

        $query2="select a.idUsuario,a.c_RP_Numero,a.RP_FechaCreacion,a.idSituacion,a.idTipo,a.RP_Rango,b.SI_Clave,c.TI_Clave,a.idReporte,a.RP_Balance from RPT a inner join SIT b on(a.idSituacion=b.idSituacion) inner join TIP c on(a.idTipo=c.idTipo) where a.idCliente=$idCliente order by a.c_RP_Numero ";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $detalle = $this->ODBC->getResultAssoc();

        $temp = array();
        foreach ($detalle['data'] as $value) {
            if(!isset($temp[$value['c_RP_Numero']])){
            $temp[$value['c_RP_Numero']] = array();
            }
            array_push($temp[$value['c_RP_Numero']],$value);
        }
        return $this->render('MovimientoMovimientoBundle:Default:ver_movimientos_view.html.twig', array('cliente' => $cliente, 'detalle' => $temp, 'idUsuario' => $idUsuario, 'idCliente' => $idCliente));
    }//ver movimientos de los clientes
}