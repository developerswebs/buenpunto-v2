<?php

namespace Utilerias\ConfigurationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ConfigurationController extends Controller {

    protected $App = array(
        '_id_Edicion'   => 1,
        'open'          => TRUE,
        'mantenimiento' => FALSE,
        'ga'            => FALSE,
        'notity_error_server' => TRUE,
        'email_unico'   => FALSE,
        'msapi'         => FALSE,
        'allow_cupon'   => FALSE, /* Funcionalidad de los Cupones */
        'salt'          => '*;7/SjqjVjIsI*',
        'Evento_es'     => 'buenpunto.com',
        'Evento_en'     => 'bp 2014 Event',
        'link_oficial_es'     => '',
        'link_oficial_en'     => '',
        'cierre_automatico' => array(
            'activado'      => FALSE, /* activado: true se cerrará automáticamente en la fecha y hora establecida (Tiempo Centro de México) */
            'fecha_cierre'  => '10/04/2013 10:03', /* formato fecha de cierre "mm/dd/aaaa" y "hh:mm" formato 24 hrs */
        ),
        'mail_list' => array(
            'MailContacto'  => 'buenpunto@fmdatabases.com',
            'MailDebug'     => 'debug_mail@infoexpo.com.mx',
            'MailNotify'    => 'servers.infoexpo@gmail.com',
        ),
        'modulos' => array(
            'plano'  => TRUE,
            'directorio' => TRUE,
            'conferencias' => FALSE,
            'tienda' => FALSE,
            'citas_negocios' => FALSE,
        ),
        'evento' => array(
            'fecha_inicio'  => '01/01/2014',/* Fecha de inicio del evento. Formato "DD/MM/YYYY" */
            'fecha_fin' => '05/01/2014',/* Fecha termino del evento. Formato "DD/MM/YYYY" */
            'localizacion_es' => 'Centro Banamex, Distrito Federal, México.', /* Lugar del Evento es */
            'localizacion_en' => 'Banamex Center, Mexico City.', /* Lugar del Evento en */
            'descripcion_es' => '', /* Descripcion del evento es*/
            'descripcion_en' => '', /* Descripcion del evento en*/
        ),
    );
    
    protected $titles = array(
        'Arq.' => array(
            'es' => 'Arq.',
            'en' => 'Architect',
        ),
        'Ing.' => array(
            'es' => 'Ing.',
            'en' => 'Engineer',
        ),
        'Lic.' => array(
            'es' => 'Lic.',
            'en' => 'Lic.',
        ),
        'Sr.' => array(
            'es' => 'Sr.',
            'en' => 'Mr.',
        ),
        'Sra.' => array(
            'es' => 'Sra.',
            'en' => 'Mrs.',
        ),
        'Srita.' => array(
            'es' => 'Srita.',
            'en' => 'Miss',
        ),
    );
    
    protected $deposito_bancario = array(
        'Banco'         => 'Prueba Banco',
        'RazonSocial'   => 'Prueba BP',
        'Cuenta'        => 0123456789,
        'RazonSocial'   => 012345678912345678,
        'ClaveCobro'    => 'I',
    );

    protected $parameters_gmail_notify = array(
        'mailer_host' => 'smtp.gmail.com',
        'mailer_user' => 'servers.bp@gmail.com',
        'mailer_password' => 'bp+1420',
    );
    
    public function getApp() {
        return $this->App;
    }
    
    public function getTitles() {
        return $this->titles;
    }
    
    public function getDepositoBancario() {
        return $this->deposito_bancario;
    }
    
    public function getParametersGmailNotify() {
        return $this->parameters_gmail_notify;
    }

}
