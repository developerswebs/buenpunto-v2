<?php

namespace Utilerias\ConfigurationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Utilerias\TextosBundle\Controller\TextosController;

//ini_set('max_input_vars', 9000);
class AdminController extends Controller {

    protected $LoginModel, $Textos;

    public function __construct() {
        //$this->LoginModel = new LoginModel();
        $this->Textos = new TextosController();
    }

    public function cacheAction() {
        $lang = 'es';
        $request = $this->getRequest();
        $session = $request->getSession();
        $session->set('lang', $lang);
        $App = $this->get('ixpo_configuration')->getApp();
        $dir = '.' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'cache';
        $this->i = 1;
        $files = $this->dirToArray($dir);

        if ($request->getMethod() == 'POST') {
            $post = $request->request->all();
            $folder_temp = 'null';
            //$path_temp = '';
            foreach ($post['items'] as $item) {
                if (strpos($item[2], $folder_temp) === false) {
                    $folder_temp = $item[2];
                    if (is_dir($item[2])) {
                        $this->delete_on_cascade($item[2]);
                        //rmdir($item[2]);
                    } else {
                        unlink($item[2]);
                    }
                }
            }
            $response = new Response(json_encode(array('staus' => TRUE, 'data' => '')));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        $content = array();
        $content['App'] = $App;
        $content['textosGenerales'] = $this->Textos->obtenerTextos("", $lang);
        $content['files'] = $files;
        $response = $this->render('UtileriasConfigurationBundle:Admin:cache.html.twig', array('content' => $content));
        return $response;
    }

    public function dirToArray($dir) {
        $result = array();
        $cdir = scandir($dir);
        foreach ($cdir as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
                    $result[] = array('title' => $value, 'isFolder' => TRUE, 'path' => $dir . DIRECTORY_SEPARATOR . $value, 'children' => $this->dirToArray($dir . DIRECTORY_SEPARATOR . $value));
                } else {
                    $result[] = array('title' => $value, 'path' => $dir . DIRECTORY_SEPARATOR . $value,);
                }
            }
        }

        return $result;
    }

    public function delete_on_cascade($dir) {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->delete_on_cascade("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    /* public function delete_on_cascade($dir) {
      if ($handle = opendir($dir)) {
      while ($obj = readdir($handle)) {
      if ($obj != '.' && $obj != '..') {
      if (is_dir($dir . $obj)) {
      if (!deleteDir($dir . $obj))
      return false;
      }
      elseif (is_file($dir . $obj)) {
      if (!unlink($dir . $obj))
      return false;
      }
      }
      }

      closedir($handle);

      if (!@rmdir($dir))
      return false;
      return true;
      }
      return false;
      } */
}
