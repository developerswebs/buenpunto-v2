$(document).ready(init);
var array_items = new Array();

function init() {
}

$("#deselectAll").click(function() {
    $("#tree").dynatree("getRoot").visit(function(node) {
        node.select(false);
    });
    return false;
});

$("#selectAll").click(function() {
    $("#tree").dynatree("getRoot").visit(function(node) {
        node.select(true);
    });
    return false;
});

$('#delete').click(parse_delete);
function parse_delete() {
    var selectedNodes = $("#tree").dynatree("getTree").getSelectedNodes();
    var items = 0;
    var selKeys = $.map(selectedNodes, function(node1) {
        var item_temp = new Array();        
        item_temp.push(node1.data.key);
        item_temp.push(node1.data.title);
        item_temp.push(node1.data.path);
        array_items.push(item_temp);
        items++;
    });
    $('#no_items').html(items);
    if (items === 0) {
        return;
    }
    $('#modal-cache').modal('toggle');
}

$('#delete-items').click(delete_directories);
status_find = 0;
function delete_directories() {
    $('#loader').fadeIn();
    /*if (status_find)
        return false;
    status_find = 1;*/

    $.ajax({
        type: "post",
        url: $('#form_delete').val(),
        dataType: 'json',
        data: { 'items':array_items },
        success: function(data) {
            array_items = new Array();
            $('#loader').hide();
        },
        error: function(request, status, error) {
            $('#loader').fadeOut();
            array_items = new Array();
            show_error_modal(request.responseText);
        }
    });
}