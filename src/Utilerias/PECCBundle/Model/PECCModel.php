<?php

namespace Utilerias\PECCBundle\Model;

use Utilerias\FileMakerBundle\API\ODBC\Client;
use CA\CaBundle\Model\PerfilModel; //libreria para generar el perfil del usuario

class PECCModel extends PerfilModel {

    protected $FM_PECC;

    public function __construct() {
        $this->FM_PECC = new Client('PECC');
    }

    public function getPaises($lang) {
        $lang = strtoupper($lang);
        $file = '../app/cache/bp/pecc/paises_' . $lang . '.json';
        if (!file_exists($file)) {
            $qry = '';
            $qry .= ' SELECT';
            $qry .= ' "id_Pais",';
            $qry .= ' "Pais_' . $lang . '"';
            $qry .= ' FROM Pais';
            $qry .= ' ORDER BY "Pais_' . $lang . '"';

            $this->FM_PECC->setCache(0);
            $this->FM_PECC->setQuery($qry);
            $r = $this->FM_PECC->exec();
            $result = $this->FM_PECC->getResultAssoc();

            $data = array();
            if ($result['status']) {
                if (count($result['data']) > 0) {
                    foreach ($result['data'] as $value) {
                        $data[$value['id_Pais']] = $value['Pais_' . $lang];
                    }
                    $result['data'] = $data;
                    $this->writeJSON($file, $result);
                    clearstatcache();
                }
            } else {
                throw new \Exception($result['data'], 409);
            }
        } else {
            $result_cache = file_get_contents($file);
            $result = json_decode($result_cache, TRUE);
        }
        return $result;
    }

    public function getEstados($id_Pais) {
        $file = '../app/cache/bp/pecc/estados_' . $id_Pais . '.json';

        if (!file_exists($file)) {
            $qry = '';
            $qry .= ' SELECT';
            $qry .= ' "id_Estado",';
            $qry .= ' Estado';
            $qry .= ' FROM Estado';
            $qry .= ' WHERE "id_Pais"=' . $id_Pais;
            $qry .= ' ORDER BY Estado';

            $this->FM_PECC->setQuery($qry);
            $r = $this->FM_PECC->exec();
            $result = $this->FM_PECC->getResultAssoc();

            $data = array();
            if ($result['status']) {
                if (count($result['data']) > 0) {
                    foreach ($result['data'] as $value) {
                        $data[$value['id_Estado']] = $value['Estado'];
                    }
                    $result['data'] = $data;
                    $this->writeJSON($file, $result);
                    clearstatcache();
                }
            } else {
                throw new \Exception($result['data'], 409);
            }
        } else {
            $result_cache = file_get_contents($file);
            $result = json_decode($result_cache, TRUE);
        }
        return $result;
    }

    public function getCiudades($id_Estado) {
        $file = '../app/cache/bp/pecc/ciudades_' . $id_Estado . '.json';

        if (!file_exists($file)) {
            $qry = '';
            $qry .= ' SELECT';
            $qry .= ' "id_Ciudad",';
            $qry .= ' Ciudad';
            $qry .= ' FROM Ciudad';
            $qry .= ' WHERE "id_Estado"=' . $id_Estado;

            $this->FM_PECC->setQuery($qry);
            $r = $this->FM_PECC->exec();
            $result = $this->FM_PECC->getResultAssoc();

            $data = array();
            if ($result['status']) {
                if (count($result['data']) > 0) {
                    foreach ($result['data'] as $value) {
                        $data[$value['id_Ciudad']] = $value['Ciudad'];
                    }
                    $result['data'] = $data;
                    $this->writeJSON($file, $result);
                    clearstatcache();
                }
            } else {
                throw new \Exception($result['data'], 409);
            }
        } else {
            $result_cache = file_get_contents($file);
            $result = json_decode($result_cache, TRUE);
        }
        return $result;
    }

    public function findCP($idPais, $cp, $lang) {
        if (empty($cp)) {
            return array('status' => FALSE, 'data' => 'Código Postal inválido');
        }
        $lang = strtoupper($lang);
        $file = '../app/cache/bp/pecc/pais_' . $idPais . '_cp_' . $cp . ".json";
        if (!file_exists($file)) {
            $paises = $this->getPaises($lang);
            $estados = $this->getEstados($idPais);
            $qry = '';
            $qry .= ' SELECT';
            $qry .= ' Colonia,';
            $qry .= ' CP,';
            $qry .= ' "id_Estado",';
            $qry .= ' "id_Ciudad",';
            $qry .= ' "id_Colonia_CP"';
            $qry .= ' FROM Colonia';
            $qry .= ' WHERE "id_Pais"=' . $idPais;
            $qry .= " AND CP='" . $cp . "'";

            $this->FM_PECC->setQuery($qry);
            $r = $this->FM_PECC->exec();
            $result = $this->FM_PECC->getResultAssoc();

            $data = array();
            if ($result['status'] && count($result['data']) > 0) {
                $ciudades = $this->getCiudades($result['data'][0]['id_Estado']);
                if (count($result['data']) > 0) {
                    foreach ($result['data'] as $value) {
                        $data[$value['id_Colonia_CP']] = $value;
                        $data[$value['id_Colonia_CP']]['Ciudad'] = $ciudades['data'][$value['id_Ciudad']];
                        $data[$value['id_Colonia_CP']]['Estado'] = $estados['data'][$value['id_Estado']];
                        $data[$value['id_Colonia_CP']]['Pais'] = $paises['data'][$idPais];
                        $data[$value['id_Colonia_CP']]['value'] = $value['CP'];

                        $label = $value['Colonia'] . ', ' . $ciudades['data'][$value['id_Ciudad']] . ', ' . $estados['data'][$value['id_Estado']];
                        $data[$value['id_Colonia_CP']]['label'] = $label;
                    }
                    $result['data'] = $data;
                }
                $this->writeJSON($file, $result);
                clearstatcache();
            }
        } else {
            $result_cache = file_get_contents($file);
            $result = json_decode($result_cache, TRUE);
        }
        return $result;
    }

}
