<?php

namespace Utilerias\PECCBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class PECCController extends Controller {

    public function getEstadosAction($id_Pais) {
        $response = new Response(json_encode($this->get('pecc')->getEstados($id_Pais)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    public function getCiudadesAction($id_Estado) {
        $response = new Response(json_encode($this->get('pecc')->getCiudades($id_Estado)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function getPECCAction($id_Pais) {
        $request = $this->getRequest();
        $session = $request->getSession();
        $lang = $session->get('lang');
        if(empty($lang)){
            $lang = 'es';
        }
        $cp = $request->get('term');
        $result = $this->get('pecc')->findCP($id_Pais, $cp, $lang);
        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
