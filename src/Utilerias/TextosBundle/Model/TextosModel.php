<?php

namespace Utilerias\TextosBundle\Model;

use Utilerias\FileMakerBundle\API\ODBC\Client;

class TextosModel {
    protected $FM_Client;

    public function __construct() {
        $this->FM_Client = new Client();
    }

    public function obtenerTextos($Args) {
        $this->FM_Client->setCache(0);
        $this->FM_Client->setExpirationTime(3600);

        $qry = 'SELECT * FROM Texto' . $this->buildCondition($Args);
        $this->FM_Client->setQuery($qry);

        $this->FM_Client->exec();
        return $this->FM_Client->getResultAssoc();
    }
    
    public function obtenerTextosValidacion($Args = '') {
        $this->FM_Client->setCache(0);
        $this->FM_Client->setExpirationTime(3600);

        $qry = 'SELECT * FROM Validacion' . $this->buildCondition($Args);
        $this->FM_Client->setQuery($qry);

        $this->FM_Client->exec();
        return $this->FM_Client->getResultAssoc();
    }

    public function buildCondition($Args){
        $condicion = '';
        if(is_array($Args)){
            $condicion .= ' WHERE ';
        } else {
            return '';
        }
        $i = 1;
        foreach ($Args as $key => $value) {
            $condicion.= $key;
            $condicion.= ($value == "")?" IS NULL ":" = " . $value;
            if($i < COUNT($Args)){ $condicion.= ' AND '; }
            $i++;
        }
        return $condicion;
    }
}