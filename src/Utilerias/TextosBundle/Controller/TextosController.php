<?php

namespace Utilerias\TextosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Utilerias\TextosBundle\Model\TextosModel;

class TextosController extends Controller {

    protected $TextosModel;

    public function __construct() {
        $this->TextosModel = new TextosModel();
    }

    public function obtenerTextos($seccion = "", $lang = 'ES') {
        $lang = ($lang == "")?'ES':strtoupper($lang);
        $textos = null;
        if ($seccion == 0 || $seccion == "") {
            $seccion = "";
            $ruta = '../app/cache/textos/seccion_TextosGenerales_' . $lang . '.json';
        } else {
            $ruta = '../app/cache/textos/seccion_' . $seccion . '_' . $lang . '.json';
        }

        if (!file_exists($ruta)) {
            $Args = array(
                '"_id_Seccion"' => $seccion,
                '"_id_Forma"' => '',
            );
            $result = $this->TextosModel->obtenerTextos($Args);

            if ($result['status']) {
                if (count($result['data']) > 0) {
                    foreach ($result['data'] as $texto) {
                        $textos[$texto['Nombre_Campo']] = $texto['Texto_' . $lang];
                    }
                    $this->writeJSON($ruta, $textos);
                    clearstatcache();
                }
            } else {
                throw new \Exception($result['data'], 409);
            }
        } else {
            $result_cache = file_get_contents($ruta);
            $textos = json_decode($result_cache, TRUE);
        }

        return $textos;
    }

    public function obtenerTextosAction($seccion, $lang) {
        $textos = $this->obtenerTextos($seccion, $lang);
        $response = new Response(json_encode(array('textos' => $textos)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function obtenerTextosValidacion($lang = 'ES') {
        $lang = ($lang == "")?'ES':strtoupper($lang);
        $textos = NULL;
        $ruta = '../app/cache/textos/textos_validacion_' . $lang . '.json';

        if (!file_exists($ruta)) {
            $result = $this->TextosModel->obtenerTextosValidacion();

            if ($result['status']) {
                if (count($result['data']) > 0) {
                    foreach ($result['data'] as $r) {
                        $textos[$r['Validacion']] = $r['Mensaje_' . strtoupper($lang)];
                    }
                    $this->writeJSON($ruta, $textos);
                    clearstatcache();
                }
            } else {
                throw new \Exception($result['data'], 409);
            }
        } else {
            $result_cache = file_get_contents($ruta);
            $textos = json_decode($result_cache, TRUE);
        }
        return $textos;
    }

    public function obtenerTextosValidacionAction($lang = 'ES') {
        $textosValidacion = $this->obtenerTextosValidacion($lang);
        $response = new Response(json_encode(array('messages' => $textosValidacion)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    private function writeJSON($filename, $array) {
        $json = json_encode($array);
        $fp = fopen($filename, "w");
        fwrite($fp, $json);
        fclose($fp);
    }

}
