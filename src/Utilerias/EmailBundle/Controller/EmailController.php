<?php

namespace Utilerias\EmailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Utilerias\ConfigurationBundle\Controller\ConfigurationController;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class EmailController extends Controller {

    protected $container;

    const APP = 'Attendee Experience / Floor Plan';

    public function __construct(ContainerInterface $container = null) {
        $this->container = $container;
    }

    /**
     * 
     * @param type $subject Asunto del correo
     * @param type $to Opcional un sólo correo o arreglo de correos
     * @param type $body twig renderizado del mensaje
     * @param type $lang lenguale, por defecto Español
     * @return type Mailer Object
     */
    public function send_email($subject, $to, $body, $lang = 'es') {
        $request = $this->getRequest();
        $session = $request->getSession();
        $lang_session = $session->get('lang');

        if (isset($lang_session)) {
            $lang = $lang_session;
        }

        $App = $this->get('ixpo_configuration')->getApp();
        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom(array($App['mail_list']['MailContacto'] => $App['Evento_' . $lang]))
                ->setTo($to)
                ->setBcc($App['mail_list']['MailDebug'])
                ->setBody($body, 'text/html');

        return $this->get('mailer')->send($message);
    }

    /**
     * 
     * @param type $error Error message
     * @param type $qry Query excecuted
     * @param type $API ODBC (Default) / FMAPI
     */
    public function notify_server_error($error = "", $qry = "", $API = 'ODBC', $error_code = "") {
        date_default_timezone_set('America/Mexico_City');
        $date = date('m/d/Y h:i:s a', time());
        $CB = new ConfigurationController();
        $App = $CB->getApp();
        $request_uri = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        if ($App['notity_error_server'] && !strstr($request_uri, 'app_dev') && !strstr($request_uri, 'localhost')) {
            $container = new ContainerBuilder();
            $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . "/../../../../app/config"));
            $loader->load('parameters.yml');
            $FileMaker_TSM = $container->getParameter('FileMaker_TSM');

            $body = '<h3>' . $API . ' server error</h3>';
            $body .= 'APP: ' . self::APP . '<br><br>';
            $body .= 'URL: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '<br>';
            if ($error_code != "") {
                $body .= 'Error Code: ' . $error_code . '<br>';
            }
            $body .= 'Error: ' . $error . '<br>';
            $body .= 'Server: ' . $FileMaker_TSM['fm_server'] . '<br>';
            $body .= 'Event:  ' . $App['Evento_es'] . '<br>';
            if ($qry != "") {
                $body .= 'Query: ' . $qry . '<br>';
            }
            $body .= 'IP: ' . $_SERVER['REMOTE_ADDR'] . '<br>';

            $ua = $this->getBrowser();
            $body .= 'Browser: ' . $ua['name'] . " | Version: " . $ua['version'] . " | OS: " . $ua['platform'] . '<br>';
            $body .= '<label style="font-size: .8em;font-style: italic;">' . $ua['userAgent'] . '</label><br>';
            $body .= 'Date:   ' . $date;

            $gm_param = $CB->getParametersGmailNotify();

            try {
                $transport = \Swift_SmtpTransport::newInstance($gm_param['mailer_host'], 465, 'ssl')->setUsername($gm_param['mailer_user'])->setPassword($gm_param['mailer_password']);
                $mailer = \Swift_Mailer::newInstance($transport);
                $message = \Swift_Message::newInstance()
                        ->setSubject($API . ' server error ' . $FileMaker_TSM['fm_server'])
                        ->setFrom($gm_param['mailer_user'])
                        ->setTo($App['mail_list']['MailNotify'])
                        ->setBody($body, 'text/html');
                $result = $mailer->send($message);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    private function getBrowser() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version = "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason

        if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Trident/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Internet Explorer 11';
            $ub = "Trident";
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/Chrome/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Opera/i', $u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        } else {
            $bname = 'Unknown';
            $ub = "Unknown";
        }

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
                ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                $version = isset($matches['version'][0]) ? $matches['version'][0] : '';
            } else {
                $version = (isset($matches['version'][1])) ? $matches['version'][1] : '';
            }
        } else {
            $version = isset($matches['version'][0]) ? $matches['version'][0] : '';
        }

        // check if we have a number
        if ($version == null || $version == "") {
            $version = "?";
        }

        return array(
            'userAgent' => $u_agent,
            'name' => $bname,
            'version' => $version,
            'platform' => $platform,
            'pattern' => $pattern
        );
    }

}
