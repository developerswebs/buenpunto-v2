<?php

namespace Utilerias\HandleErrorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Utilerias\EmailBundle\Controller\EmailController;
use Utilerias\ConfigurationBundle\Controller\ConfigurationController;

class ExceptionController extends Controller {

    protected $templating;

    public function __construct(TwigEngine $templating = null) {
        $this->templating = $templating;
    }

    public function onKernelException(GetResponseForExceptionEvent $event) {
        return $this->defaultError($event);
    }

    public function defaultError(GetResponseForExceptionEvent $event) {
        // We get the exception object from the received event
        $exception = $event->getException();
        $request = $event->getRequest();
        $session = $request->getSession();
        $lang = $session->get('lang');
        $ruta = __DIR__ . '/../Resources/private/text.json';
        $result_cache = file_get_contents($ruta);
        $textos = json_decode($result_cache, TRUE);

        $content = array();
        $content['lang'] = ($lang != "") ? $lang : 'es';
        $content['textos'] = $textos;
        $error_code = $exception->getCode();
        $content['code'] = ($error_code != 0) ? $error_code : $exception->getStatusCode();
        $content['referer'] = $request->headers->get('referer');
        $content['file'] = $event->getException()->getFile();
        $content['line'] = $event->getException()->getLine();
        $content['message'] = $exception->getMessage();
        $CB = new ConfigurationController();
        $App = $CB->getApp();
        if ($App['notity_error_server'] && !strstr($request->getRequestUri(), 'app_dev') && !strstr($request->getRequestUri(), 'localhost')) {
            /* Notificación del Error */
            $EM = new EmailController();
            $error_message = $content['message'] . ' | En ' . $content['file'] . ' linea ' . $content['line'];
            $EM->notify_server_error($error_message, "", 'Web Code', $content['code']);
        }
        $body = $this->templating->render('UtileriasHandleErrorBundle:Exception:error.html.twig', array("content" => $content));
        $response = new Response($body);
        $event->setResponse($response);
    }

    public function customtErrorAction($msg) {

        return $this->render('UtileriasHandleErrorBundle:Exception:error.html.twig', array(
                    "error_code" => "Something was wrong",
                    "sorry_message" => "",
                    "error_description" => $msg
        ));
    }

    public function indexAction($name) {
        return $this->render('UtileriasHandleErrorBundle:Default:index.html.twig', array('name' => $name));
    }

}
