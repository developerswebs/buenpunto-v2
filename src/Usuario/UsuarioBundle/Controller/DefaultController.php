<?php

namespace Usuario\UsuarioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Utilerias\FileMakerBundle\API\ODBC\Client; //libreria para conexion con ODBC, para querys
use Utilerias\FileMakerBundle\API\FM11API; //libreria para conexion a FM, para insert, delete, update

class DefaultController extends Controller
{
    private $ODBC = null;
    private $FM = null;

    public function __construct() { //constructor para usar usar al objeto FM y poder utilizar las clases de la libreria ODBC
        $this->ODBC = new Client();
        $this->FM = new FM11API();
    } //CONSTRUCTORES PARA LIBRERIAS

    public function usuarioAction()
    {
    	$query="select * from USU order by US_NivelAutoridad";
    	$this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        return $this->render('UsuarioUsuarioBundle:Default:usuario.html.twig', array('usuarios' => $result ));
    }
    public function agregar_usuarioAction()
    {
        $query="select * from DIST order by idDistribuidor";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        return $this->render('UsuarioUsuarioBundle:Default:agregar_usuario.html.twig', array('distribuidor' => $result ));
    }//mostrar pagina de agregar

    public function addUsuarioAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        //obtener idUsuario
        $request = $this->getRequest();
        if ($request->getMethod() == 'POST')
        {
            $id = $request->get('idUsuario');
            $nom = $this->limpiar($request->get('nom'));
            $ap = $this->limpiar($request->get('ap'));
            $am = $this->limpiar($request->get('am'));
            $email = $request->get('email');
            $distribuidor = $this->limpiar($request->get('distribuidor'));
            $autoridad = $this->limpiar($request->get('autoridad'));
            $status = $this->limpiar($request->get('status'));
        }
            $passwordRandom = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) .  chr(rand(65,90)) . rand(10, 90); //random 6 caracteres
            $passSHA=sha1($passwordRandom); //encriptado de contraseña nueva

        $data= null;
        $SParr = null;
        $SParr .= "idUsuario: " . $id . "; ";
        $SParr .= "US_Nombre: " . $nom . "; ";
        $SParr .= "US_ApellidoPaterno: " . $ap . "; ";
        $SParr .= "US_ApellidoMaterno: " . $am . "; ";
        $SParr .= "US_Email: " . $email . "; ";
        $SParr .= "idDistribuidor: " . $distribuidor . "; ";
        $SParr .= "US_NivelAutoridad: " . $autoridad . "; ";
        $SParr .= "US_Status: " . $status . "; ";
        $SParr .= "US_Password: " . $passSHA . "; ";
        $SParr = str_replace('"', '\"', $SParr);

        $data = $this->FM->Run_Script('PHP_USU', 'PHP_USU_Insert_Update', $SParr);
        $subject = "Activacion de Cuenta Buen Punto";
        
        if($id==0) //si es un usuario nuevo manda correo, si es update no mandes nada
        {//enviar correo de activacion
            $body = $this->renderView('UsuarioUsuarioBundle:Default:activar_cuenta.html.twig', array('token' => $passSHA));
            $res = $this->get('ixpo_mailer')->send_email($subject, $email, $body);
        }

        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
        }    
        return $this->redirect($this->generateUrl('usuario')); 
    }//agregar usuario

    public function detalle_usuarioAction($idUsuario)
    {
        $request = $this->getRequest();
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idUsuario');
        }

        $query="select * from USU where idUsuario=$id ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();

        $query2="select a.idDistribuidor, a.DI_Nombre, a.DI_RFC from DIST a inner join USU b on(a.idDistribuidor=b.idDistribuidor) where b.idUsuario=$id ";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $result2 = $this->ODBC->getResultAssoc();
            //verificar este query
        return $this->render('UsuarioUsuarioBundle:Default:detalle_usuario.html.twig', array('usuario' => $result, 'distribuidor' => $result2 ));
    }//mostrar detalle usuario

    public function editar_usuarioAction($idUsuario)
    {
        $request = $this->getRequest();
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idUsuario');
        }
        $query="select * from DIST order by idDistribuidor ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();

        $query2="select * from USU where idUsuario=$id ";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $result2 = $this->ODBC->getResultAssoc();

        return $this->render('UsuarioUsuarioBundle:Default:editar_usuario.html.twig', array('distribuidor' => $result, 'usuario' => $result2 ));
    }//mostrar editar_usuario

    public function activa_cuentaAction($token)
    {
        return $this->render('UsuarioUsuarioBundle:Default:activa.html.twig', array('token' => $token));
    }//activar cuenta
    public function activa_cuenta2Action()
    {
        $request = $this->getRequest();
        if ($request->getMethod() == 'POST')
        {
            $token = $request->get('token');
            $usuario = $request->get('usr');
            $pass = $request->get('pass');
        }
        $passSHA=sha1($pass); //encriptado de contraseña nueva
        
        $data= null;
        $SParr = null;
        $SParr .= "token: " . $token . "; ";
        $SParr .= "US_Usuario: " . $usuario . "; ";
        $SParr .= "US_Password: " . $passSHA . "; ";
        $SParr = str_replace('"', '\"', $SParr);

        $data = $this->FM->Run_Script('PHP_USU', 'PHP_USU_Activa_Cuenta', $SParr);
        return $this->redirect($this->generateUrl('logout'));
    }
    public function limpiar($msj)
    {
        $msj = str_replace(
        array("\\", "¨", "º", "-", "~",
        "#", "|", "!", "\"",
        "·", "$", "%", "&", "/",
        "(", ")", "?", "'", "¡",
        "¿", "[", "^", "`", "]",
        "+", "}", "{", "¨", "´",
        ">", "< ", ";", ",", ":",
        "."),
        '',
        $msj
        );
        return $msj;
    }
}//cierre de clase}