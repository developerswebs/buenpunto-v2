<?php

namespace Representante\RepresentanteBundle\Controller;
use Utilerias\FileMakerBundle\API\ODBC\Client; //libreria para conexion con ODBC, para querys
use Utilerias\FileMakerBundle\API\FM11API;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    private $ODBC = null;
    private $FM = null;

    public function __construct() { //constructor para usar usar al objeto FM y poder utilizar las clases de la libreria ODBC
        $this->ODBC = new Client();
        $this->FM = new FM11API();
    } //CONSTRUCTORES PARA LIBRERIAS
    
    public function representanteAction()
    {
        $query="select * from REP order by RE_Nombre";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        return $this->render('RepresentanteRepresentanteBundle:Default:representante.html.twig',array('representante'=>$result));
    }
    
    public function agregar_representanteAction()
    {
        //arreglo pasa tipos de sociedades
        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');
        return $this->render('RepresentanteRepresentanteBundle:Default:agregar_representante.html.twig',array('paises' => $result_paises));
    }
    
    public function addrepresentanteAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        //obtener idUsuario
         if ($request->getMethod() == 'POST')
        {
            $accion = $request->get('accion');
            $id = $request->get('idRepresentante');
            $nom = $this->limpiar($request->get('inputNom'));
            $pat = $this->limpiar($request->get('inputPat'));
            $mat =  $this->limpiar($request->get('inputMat'));
            $rfc =  $this->limpiar($request->get('inputRFC'));
            $cargo =  $this->limpiar($request->get('inputCargo'));
            $email =  $request->get('inputEmail');
            $tel = preg_replace('([^A-Za-z0-9])', ' ', $request->get('inputTel'));
            $tel2 = preg_replace('([^A-Za-z0-9])', ' ', $request->get('inputTel2'));
            $pais = preg_replace('([^A-Za-z0-9])', ' ', $request->get('DE_id_Pais'));
            $estado = preg_replace('([^A-Za-z0-9])', ' ', $request->get('DE_id_Estado'));
            $ciudad = preg_replace('([^A-Za-z0-9])', ' ', $request->get('DE_id_Ciudad'));
            $col =  $this->limpiar($request->get('col'));
            $calle =  $this->limpiar($request->get('calle'));           
            $cp =  $this->limpiar($request->get('inputCP'));
            $trs =  $this->limpiar($request->get('trs'));
        }
       
        $data= null;
        $SParr = null;
        $SParr .= "accion: " . $accion . "; ";
        $SParr .= "idRepresentante: " . $id . "; ";
        $SParr .= "RE_Nombre: " . $nom . "; ";
        $SParr .= "RE_RFC: " . $rfc . "; ";
        $SParr .= "RE_ApellidoPaterno: " . $pat . "; ";
        $SParr .= "RE_ApellidoMaterno: " . $mat . "; ";
        $SParr .= "RE_Cargo: " . $cargo . "; ";
        $SParr .= "RE_Email: " . $email . "; ";
        $SParr .= "RE_Telefono: " . $tel . "; ";
        $SParr .= "RE_Telefono2: " . $tel2 . "; ";
        $SParr .= "_id_Pais: " . $pais . "; ";
        $SParr .= "_id_Estado: " . $estado . "; ";
        $SParr .= "_id_Ciudad: " . $ciudad . "; ";
        $SParr .= "DR_Colonia: " . $col. "; ";
        $SParr .= "DR_Calle: " . $calle . "; ";
        $SParr .= "DR_CP: " . $cp . "; ";
        $SParr .= "idUsuario: " . $idUsuario . "; ";
        $SParr = str_replace('"', '\"', $SParr);
        
        $data = $this->FM->Run_Script('PHP_REP', 'PHP_REP_Insert_Update', $SParr);
        
        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
        }  
        
        return $this->representanteAction();
    }
    
    public function editar_representanteAction($idRepresentante)
    {
        $request = $this->getRequest();
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idRepresentante');
        }
        $query="select * from REP where idRepresentante = $id";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        
        return $this->render('RepresentanteRepresentanteBundle:Default:editar_representante.html.twig',array('datos_representante'=>$result));
    }
    
    public function detalle_representanteAction()
    {
        
        $request = $this->getRequest();
        
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idRepresentante');
        }
        
        $query="select * from REP where idRepresentante = $id";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        
                
        $query2="select * from DIREP where idRepresentante = $id";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $result2 = $this->ODBC->getResultAssoc();
        
        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');
       
        return $this->render('RepresentanteRepresentanteBundle:Default:detalle_representante.html.twig',array('datos_rep' => $result,'datos_dir'=>$result2, 'paises' => $result_paises ));
    }
    
    public function detalle_representante_2Action($id)
    {
        
        $request = $this->getRequest();
        
        $query="select * from REP where idRepresentante = $id";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        
                
        $query2="select * from DIREP where idRepresentante = $id";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $result2 = $this->ODBC->getResultAssoc();
        
        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');
       
        return $this->render('RepresentanteRepresentanteBundle:Default:detalle_representante.html.twig',array('datos_rep' => $result,'datos_dir'=>$result2, 'paises' => $result_paises ));
    }
    
    public function agregar_direccion_representanteAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        
        if ($request->getMethod() == 'POST')
        {
            $accion = $request->get('accion');
            $id = $request->get('idRepresentante');
            $id_Dir_Rep = $request->get('id_Dir_Rep');
            $pais = $this->limpiar($request->get('DE_id_Pais'));
            $estado = $this->limpiar($request->get('DE_id_Estado'));
            $ciudad = $this->limpiar($request->get('DE_id_Ciudad'));
            $col = $this->limpiar($request->get('col'));
            $calle = $this->limpiar($request->get('calle'));           
            $cp = $this->limpiar($request->get('inputCP'));
        }
        
        if ($id_Dir_Rep != null) {
            $data = null;
            $SParr = null;
            $SParr .= "accion: " . $accion . "; ";
            $SParr .= "idDireccionRepresentante: " . $id_Dir_Rep . "; ";
            $SParr .= "idUsuario: " . $idUsuario . "; ";
            $SParr .= "idRepresentante: " . $id . "; ";
            $SParr .= "_id_Pais: " . $pais . "; ";
            $SParr .= "_id_Estado: " . $estado . "; ";
            $SParr .= "_id_Ciudad: " . $ciudad . "; ";
            $SParr .= "DR_Colonia: " . $col . "; ";
            $SParr .= "DR_Calle: " . $calle . "; ";
            $SParr .= "DR_CP: " . $cp . "; ";
            
            $data = $this->FM->Run_Script('PHP_DIREP', 'PHP_DIREP_Insert_Update', $SParr);
            
             if($data == null)
            {
                $session->getFlashBag()->add('flash1', 'Surgio un error');
            }
            else
            {
                $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
            }
            
            return $this->detalle_representante_2Action($id);
            
        } else {
            $data = null;
            $SParr = null;
            $SParr .= "accion: " . $accion . "; ";
            $SParr .= "idUsuario: " . $idUsuario . "; ";
            $SParr .= "idRepresentante: " . $id . "; ";
            $SParr .= "_id_Pais: " . $pais . "; ";
            $SParr .= "_id_Estado: " . $estado . "; ";
            $SParr .= "_id_Ciudad: " . $ciudad . "; ";
            $SParr .= "DR_Colonia: " . $col . "; ";
            $SParr .= "DR_Calle: " . $calle . "; ";
            $SParr .= "DR_CP: " . $cp . "; ";
            
            $data = $this->FM->Run_Script('PHP_DIREP', 'PHP_DIREP_Insert_Update', $SParr);
            
            if($data == null)
            {
                $session->getFlashBag()->add('flash1', 'Surgio un error');
            }
            else
            {
                $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
            }
            
            return $this->detalle_representante_2Action($id);
        }
    }
    
    public function editar_direccion_representanteAction($idRep,$idDir)
    {
        $request = $this->getRequest();
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        
        $query="select * from REP where idRepresentante = $idRep";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
      
        $query2="select * from DIREP where idRepresentante = $idRep";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $result2 = $this->ODBC->getResultAssoc();
        

        $query3="select * from DIREP where idDireccionRepresentante = $idDir";
        $this->ODBC->setQuery($query3);
        $this->ODBC->exec();
        $result3 = $this->ODBC->getResultAssoc();   
        
        $idPais=$result3['data'][0]['_id_Pais'];
        $idEstado=$result3['data'][0]['_id_Estado'];
        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');
        $reult_estados = $PECC->getEstados($idPais);
        $reult_ciudades = $PECC->getCiudades($idEstado);
        
        return $this->render('RepresentanteRepresentanteBundle:Default:editar_direccion_representante.html.twig',array('datos_rep' => $result,'datos_dir'=>$result2,'datos_dir_edit'=>$result3, 'paises' => $result_paises, 'estados' => $reult_estados, 'ciudades' => $reult_ciudades ));
    }
    
    public function ver_clientes_representanteAction()
    {
        $request = $this->getRequest();
        
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idRepresentante');
        }
        
        return $this->redirect($this->generateUrl('detalle_cliente',array('idCliente' => $id)));
    }
    //USER//
    public function representante_userAction()
    {
        $query="select * from REP order by RE_Nombre";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        return $this->render('RepresentanteRepresentanteBundle:Default:representante_user.html.twig',array('representante'=>$result));
    }

    public function agregar_representante_userAction()
    {
        //arreglo pasa tipos de sociedades
        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');
        return $this->render('RepresentanteRepresentanteBundle:Default:agregar_representante_user.html.twig',array('paises' => $result_paises));
    }
    public function addrepresentante_userAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        //obtener idUsuario
         if ($request->getMethod() == 'POST')
        {
            $accion = $request->get('accion');
            $id = $request->get('idRepresentante');
            $nom = $this->limpiar($request->get('inputNom'));
            $pat = $this->limpiar($request->get('inputPat'));
            $mat =  $this->limpiar($request->get('inputMat'));
            $rfc =  $this->limpiar($request->get('inputRFC'));
            $cargo =  $this->limpiar($request->get('inputCargo'));
            $email =  $request->get('inputEmail');
            $tel = preg_replace('([^A-Za-z0-9])', ' ', $request->get('inputTel'));
            $tel2 = preg_replace('([^A-Za-z0-9])', ' ', $request->get('inputTel2'));
            $pais = preg_replace('([^A-Za-z0-9])', ' ', $request->get('DE_id_Pais'));
            $estado = preg_replace('([^A-Za-z0-9])', ' ', $request->get('DE_id_Estado'));
            $ciudad = preg_replace('([^A-Za-z0-9])', ' ', $request->get('DE_id_Ciudad'));
            $col =  $this->limpiar($request->get('col'));
            $calle =  $this->limpiar($request->get('calle'));           
            $cp =  $this->limpiar($request->get('inputCP'));
            $trs =  $this->limpiar($request->get('trs'));
        }
       
        $data= null;
        $SParr = null;
        $SParr .= "accion: " . $accion . "; ";
        $SParr .= "idRepresentante: " . $id . "; ";
        $SParr .= "RE_Nombre: " . $nom . "; ";
        $SParr .= "RE_RFC: " . $rfc . "; ";
        $SParr .= "RE_ApellidoPaterno: " . $pat . "; ";
        $SParr .= "RE_ApellidoMaterno: " . $mat . "; ";
        $SParr .= "RE_Cargo: " . $cargo . "; ";
        $SParr .= "RE_Email: " . $email . "; ";
        $SParr .= "RE_Telefono: " . $tel . "; ";
        $SParr .= "RE_Telefono2: " . $tel2 . "; ";
        $SParr .= "_id_Pais: " . $pais . "; ";
        $SParr .= "_id_Estado: " . $estado . "; ";
        $SParr .= "_id_Ciudad: " . $ciudad . "; ";
        $SParr .= "DR_Colonia: " . $col. "; ";
        $SParr .= "DR_Calle: " . $calle . "; ";
        $SParr .= "DR_CP: " . $cp . "; ";
        $SParr .= "idUsuario: " . $idUsuario . "; ";
        $SParr = str_replace('"', '\"', $SParr);
        
        $data = $this->FM->Run_Script('PHP_REP', 'PHP_REP_Insert_Update', $SParr);
        
        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
        }  
        
        return $this->representante_userAction();
    }
    
    public function detalle_representante_userAction()
    {
        
        $request = $this->getRequest();
        
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idRepresentante');
        }
        
        $query="select * from REP where idRepresentante = $id";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        
                
        $query2="select * from DIREP where idRepresentante = $id";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $result2 = $this->ODBC->getResultAssoc();
        
        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');
       
        return $this->render('RepresentanteRepresentanteBundle:Default:detalle_representante_user.html.twig',array('datos_rep' => $result,'datos_dir'=>$result2, 'paises' => $result_paises ));
    }
    
    public function detalle_representante_2_userAction($id)
    {
        
        $request = $this->getRequest();
        
        $query="select * from REP where idRepresentante = $id";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        
                
        $query2="select * from DIREP where idRepresentante = $id";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $result2 = $this->ODBC->getResultAssoc();
        
        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');
       
        return $this->render('RepresentanteRepresentanteBundle:Default:detalle_representante_user.html.twig',array('datos_rep' => $result,'datos_dir'=>$result2, 'paises' => $result_paises ));
    }
    
    public function agregar_direccion_representante_userAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        
        if ($request->getMethod() == 'POST')
        {
            $accion = $request->get('accion');
            $id = $request->get('idRepresentante');
            $id_Dir_Rep = $request->get('id_Dir_Rep');
            $pais = $this->limpiar($request->get('DE_id_Pais'));
            $estado = $this->limpiar($request->get('DE_id_Estado'));
            $ciudad = $this->limpiar($request->get('DE_id_Ciudad'));
            $col = $this->limpiar($request->get('col'));
            $calle = $this->limpiar($request->get('calle'));           
            $cp = $this->limpiar($request->get('inputCP'));
        }
        
        if ($id_Dir_Rep != null) {
            $data = null;
            $SParr = null;
            $SParr .= "accion: " . $accion . "; ";
            $SParr .= "idDireccionRepresentante: " . $id_Dir_Rep . "; ";
            $SParr .= "idUsuario: " . $idUsuario . "; ";
            $SParr .= "idRepresentante: " . $id . "; ";
            $SParr .= "_id_Pais: " . $pais . "; ";
            $SParr .= "_id_Estado: " . $estado . "; ";
            $SParr .= "_id_Ciudad: " . $ciudad . "; ";
            $SParr .= "DR_Colonia: " . $col . "; ";
            $SParr .= "DR_Calle: " . $calle . "; ";
            $SParr .= "DR_CP: " . $cp . "; ";
            
            $data = $this->FM->Run_Script('PHP_DIREP', 'PHP_DIREP_Insert_Update', $SParr);
            
             if($data == null)
            {
                $session->getFlashBag()->add('flash1', 'Surgio un error');
            }
            else
            {
                $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
            }
            
            return $this->detalle_representante_2_userAction($id);
            
        } else {
            $data = null;
            $SParr = null;
            $SParr .= "accion: " . $accion . "; ";
            $SParr .= "idUsuario: " . $idUsuario . "; ";
            $SParr .= "idRepresentante: " . $id . "; ";
            $SParr .= "_id_Pais: " . $pais . "; ";
            $SParr .= "_id_Estado: " . $estado . "; ";
            $SParr .= "_id_Ciudad: " . $ciudad . "; ";
            $SParr .= "DR_Colonia: " . $col . "; ";
            $SParr .= "DR_Calle: " . $calle . "; ";
            $SParr .= "DR_CP: " . $cp . "; ";
            
            $data = $this->FM->Run_Script('PHP_DIREP', 'PHP_DIREP_Insert_Update', $SParr);
            
            if($data == null)
            {
                $session->getFlashBag()->add('flash1', 'Surgio un error');
            }
            else
            {
                $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
            }
            
            return $this->detalle_representante_2_userAction($id);
        }
    }
    public function ver_clientes_representante_userAction()
    {
        $request = $this->getRequest();
        
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idRepresentante');
        }
        
        return $this->redirect($this->generateUrl('detalle_cliente_user',array('idCliente' => $id)));
    }
    //USER//
    public function representante_viewAction()
    {
        $query="select * from REP order by RE_Nombre";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        return $this->render('RepresentanteRepresentanteBundle:Default:representante_view.html.twig',array('representante'=>$result));
    }
    public function ver_clientes_representante_viewAction()
    {
        $request = $this->getRequest();
        
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idRepresentante');
        }
        
        return $this->redirect($this->generateUrl('detalle_cliente_view',array('idCliente' => $id)));
    }

    public function detalle_representante_viewAction()
    {
        
        $request = $this->getRequest();
        
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idRepresentante');
        }
        
        $query="select * from REP where idRepresentante = $id";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        
                
        $query2="select * from DIREP where idRepresentante = $id";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $result2 = $this->ODBC->getResultAssoc();
        
        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');
       
        return $this->render('RepresentanteRepresentanteBundle:Default:detalle_representante_view.html.twig',array('datos_rep' => $result,'datos_dir'=>$result2, 'paises' => $result_paises ));
    }
    
    public function limpiar($msj)
    {
        $msj = str_replace(
        array("\\", "¨", "º", "-", "~",
        "#", "|", "!", "\"",
        "·", "$", "%", "&", "/",
        "(", ")", "?", "'", "¡",
        "¿", "[", "^", "`", "]",
        "+", "}", "{", "¨", "´",
        ">", "< ", ";", ",", ":",
        "."),
        '',
        $msj
        );
        return $msj;
    }
}
