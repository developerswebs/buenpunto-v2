<?php

namespace Cliente\ClienteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Utilerias\FileMakerBundle\API\ODBC\Client; //libreria para conexion con ODBC, para querys
use Utilerias\FileMakerBundle\API\FM11API; //libreria para conexion a FM, para insert, delete, update

class DefaultController extends Controller
{
    private $ODBC = null;
    private $FM = null;

    public function __construct() { //constructor para usar usar al objeto FM y poder utilizar las clases de la libreria ODBC
        $this->ODBC = new Client();
        $this->FM = new FM11API();
    } //CONSTRUCTORES PARA LIBRERIAS

    public function clienteAction()
    {
        $query="select * from CTE order by idCliente";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        return $this->render('ClienteClienteBundle:Default:cliente.html.twig', array('clientes' => $result ));
    }
    public function agregar_clienteAction()
    {
        //arreglo pasa tipos de sociedades
        $tipoSoc['tipoSoc']=array('Persona Física','A.C.','CO.','CO. LTD','GMBH','S.A.','S.A. DE C.V.','S.A.P.I. DE C.V.','S. DE R.L. DE C.V.','S. DE R.L. MI','S.C.','S.C.L.','S.P.A.','S.L.','L.T.D.','LTDA','L.L.C.','S.R.L.','INC.');
        //arreglo pasa tipos de sociedades
        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');

        return $this->render('ClienteClienteBundle:Default:agregar_cliente.html.twig', array('tipoSoc' => $tipoSoc, 'paises' => $result_paises ));
    }//mostrar vista de agregar
    public function addClienteAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        $idDistribuidor=$perfil['idDistribuidor'];
        //obtener idUsuario
        $request = $this->getRequest();
        if ($request->getMethod() == 'POST')
        {
            $id = $request->get('idCliente');
            $accion = $request->get('accion');
            $nom = $this->limpiar($request->get('inputNom'));
            $rfc = $this->limpiar($request->get('inputRFC'));
            $email = $this->limpiar($request->get('email'));
            $tel = $this->limpiar($request->get('tel'));
            $tel2 = $this->limpiar($request->get('tel2'));
            $calle = $this->limpiar($request->get('calle'));
            $col = $this->limpiar($request->get('col'));
            $pais = $this->limpiar($request->get('DE_id_Pais'));
            $estado = $this->limpiar($request->get('DE_id_Estado'));
            $ciudad = $this->limpiar($request->get('DE_id_Ciudad'));
            $cp = $this->limpiar($request->get('inputCP'));
            $rs = $this->limpiar($request->get('rs'));
            $trs = $this->limpiar($request->get('trs'));
        }
        $data= null;
        $SParr = null;
        $SParr .= "idCliente: " . $id . "; ";
        $SParr .= "CL_Nombre: " . $nom . "; ";
        $SParr .= "CL_RFC: " . $rfc . "; ";
        $SParr .= "CL_TipoSoc: " . $trs . "; ";
        $SParr .= "CL_RazonSocial: " . $rs . "; ";
        $SParr .= "CL_Email: " . $email . "; ";
        $SParr .= "CL_Telefono: " . $tel . "; ";
        $SParr .= "CL_Telefono2: " . $tel2 . "; ";
        $SParr .= "idPais: " . $pais . "; ";
        $SParr .= "idEstado: " . $estado . "; ";
        $SParr .= "idCiudad: " . $ciudad . "; ";
        $SParr .= "Colonia: " . $col . "; ";
        $SParr .= "Calle: " . $calle . "; ";
        $SParr .= "CP: " . $cp . "; ";
        $SParr .= "idUsuario: " . $idUsuario . "; ";
        $SParr .= "idDistribuidor: " . $idDistribuidor . "; ";
        $SParr .= "accion: " . $accion . "; "; //1=insertar 2=update (DIRECCIONES)
        $SParr = str_replace('"', '\"', $SParr);

        $data = $this->FM->Run_Script('PHP_DIST', 'PHP_CTE_Insert_Update', $SParr);
        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
        }    
        return $this->redirect($this->generateUrl('cliente'));
    }//agregar cliente
    public function editar_clienteAction($idCliente)
    {
        $request = $this->getRequest();
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idCliente');
        }
        $query="select * from CTE where idCliente=$id ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        //arreglo pasa tipos de sociedades
        $tipoSoc['tipoSoc']=array('Persona Física','A.C.','CO.','CO. LTD','GMBH','S.A.','S.A. DE C.V.','S.A.P.I. DE C.V.','S. DE R.L. DE C.V.','S. DE R.L. MI','S.C.','S.C.L.','S.P.A.','S.L.','L.T.D.','LTDA','L.L.C.','S.R.L.','INC.');
        //arreglo pasa tipos de sociedades
        return $this->render('ClienteClienteBundle:Default:editar_cliente.html.twig', array('cliente' => $result, 'tipoSoc' => $tipoSoc));
    }//editar cliente

    public function detalle_clienteAction($idCliente)
    {
        $request = $this->getRequest();
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idCliente');
        }
        $cte="select * from CTE where idCliente=$id ";
        $this->ODBC->setQuery($cte);
        $this->ODBC->exec();
        $cliente = $this->ODBC->getResultAssoc();

        $query="select * from DIRCTE where idCliente=$id ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $direcciones = $this->ODBC->getResultAssoc();

        $query2="select b.idRepresentante,b.RE_NombreCompleto,b.RE_RFC from CTERP a inner join REP b on(a.idRepresentante=b.idRepresentante) where a.idCliente=$id ";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $representes = $this->ODBC->getResultAssoc();

        $query3="select DISTINCT idRepresentante,RE_NombreCompleto,RE_RFC from REP order by RE_NombreCompleto ";
        $this->ODBC->setQuery($query3);
        $this->ODBC->exec();
        $representesTabla = $this->ODBC->getResultAssoc();

        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');

        return $this->render('ClienteClienteBundle:Default:detalle_cliente.html.twig', array('cliente' => $id, 'direcciones' => $direcciones, 'representantes' => $representes, 'paises' => $result_paises, 'representesTabla' => $representesTabla, 'DatosCliente' => $cliente ));
    }//detalle cliente

    public function agregar_direccion_clienteAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        //obtener idUsuario
        if ($request->getMethod() == 'POST')
        {
            $id = $request->get('idCliente');
            $accion = $request->get('accion');
            $idDireccionCliente = $request->get('idDireccionCliente');
            $pais = $this->limpiar($request->get('DE_id_Pais'));
            $estado = $this->limpiar($request->get('DE_id_Estado'));
            $ciudad = $this->limpiar($request->get('DE_id_Ciudad'));
            $cp = $this->limpiar($request->get('inputCP'));
            $col = $this->limpiar($request->get('col'));
            $calle = $this->limpiar($request->get('calle'));
        }
        $data= null;
        $SParr = null;
        $SParr .= "idCliente: " . $id . "; ";
        $SParr .= "idPais: " . $pais . "; ";
        $SParr .= "idEstado: " . $estado . "; ";
        $SParr .= "idCiudad: " . $ciudad . "; ";
        $SParr .= "Colonia: " . $col . "; ";
        $SParr .= "Calle: " . $calle . "; ";
        $SParr .= "CP: " . $cp . "; ";
        $SParr .= "idUsuario: " . $idUsuario . "; ";
        $SParr .= "idDireccionCliente: " . $idDireccionCliente . "; ";
        $SParr .= "accion: " . $accion . "; "; //1=insertar 2=update (DIRECCIONES)
        $SParr = str_replace('"', '\"', $SParr);

        $data = $this->FM->Run_Script('PHP_DIRCTE', 'PHP_DIRCTE_Insert_Update', $SParr);

        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
        }    
        return $this->redirect($this->generateUrl('detalle_cliente',array('idCliente' => $id))); 
    }//agregar direccion a cliente
    public function agregar_representante_clienteAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();

        if ($request->getMethod() == 'POST')
        {
            $id = $request->get('idCliente');
            $accion = $request->get('accion');
            $idRepresentante = $request->get('representante');
            $post = $request->request->all();
        }
        $data= null;
        $SParr = null;
        $SParr .= "idCliente: " . $id . "; ";
        $SParr .= "idRepresentante: " . $idRepresentante . "; ";
        $SParr .= "accion: " . $accion . "; "; //1=insertar 2=update (DIRECCIONES)
        $SParr = str_replace('"', '\"', $SParr);

        $data = $this->FM->Run_Script('PHP_DIRCTE', 'PHP_CTERP_Insert', $SParr);

        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
        } 
        return $this->redirect($this->generateUrl('detalle_cliente',array('idCliente' => $id)));   
    }//agregar representantes a los clientes
    
    public function modificar_detalle_clienteAction($idCliente,$idDireccion)
    {
        $request = $this->getRequest();
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idCliente');
            $idDir = $request->get('idDireccion');
        }
        $cte="select * from CTE where idCliente=$id ";
        $this->ODBC->setQuery($cte);
        $this->ODBC->exec();
        $cliente = $this->ODBC->getResultAssoc();

        $query="select * from DIRCTE where idDireccionCliente=$idDir ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $direcciones = $this->ODBC->getResultAssoc();

        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');
        $idPais=$direcciones['data'][0]['_id_Pais'];
        $idEstado=$direcciones['data'][0]['_id_Estado'];
        $reult_estados = $PECC->getEstados($idPais);
        $reult_ciudades = $PECC->getCiudades($idEstado);

        $query2="select b.idRepresentante,b.RE_NombreCompleto,b.RE_RFC from CTERP a inner join REP b on(a.idRepresentante=b.idRepresentante) where a.idCliente=$id ";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $representes = $this->ODBC->getResultAssoc();

        $query3="select DISTINCT idRepresentante,RE_NombreCompleto,RE_RFC from REP order by RE_NombreCompleto ";
        $this->ODBC->setQuery($query3);
        $this->ODBC->exec();
        $representesTabla = $this->ODBC->getResultAssoc();

        return $this->render('ClienteClienteBundle:Default:modificar_detalle_cliente.html.twig', array('cliente' => $id, 'direcciones' => $direcciones, 'representantes' => $representes, 'paises' => $result_paises, 'representesTabla' => $representesTabla, 'DatosCliente' => $cliente, 'estados' => $reult_estados, 'ciudades' => $reult_ciudades ));
    }//detalle cliente
    public function ver_cliente_representanteAction($idRepresentante)
    {
        $request = $this->getRequest();
        if ($request->getMethod() == 'GET')
        {
            $idRepresentante = $request->get('idRepresentante');
        }
        $rep="select * from REP where idRepresentante=$idRepresentante ";
        $this->ODBC->setQuery($rep);
        $this->ODBC->exec();
        $representante = $this->ODBC->getResultAssoc();
        $idCliente=$representante['data'][0]['idCliente'];

        $cte="select b.idCliente,b.CL_Nombre,b.CL_RazonSocial,b.CL_RFC from CTERP a inner join CTE b on(a.idCliente=b.idCliente) where a.idRepresentante=$idRepresentante ";
        $this->ODBC->setQuery($cte);
        $this->ODBC->exec();
        $clientes = $this->ODBC->getResultAssoc();

        return $this->render('ClienteClienteBundle:Default:cliente_representante.html.twig', array('representante' => $representante, 'clientes' => $clientes ));
    }//ver cliente representante

    //USER//
    public function cliente_userAction()
    {
        $query="select * from CTE order by idCliente";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        return $this->render('ClienteClienteBundle:Default:cliente_user.html.twig', array('clientes' => $result ));
    }
     public function agregar_cliente_userAction()
    {
        //arreglo pasa tipos de sociedades
        $tipoSoc['tipoSoc']=array('Persona Física','A.C.','CO.','CO. LTD','GMBH','S.A.','S.A. DE C.V.','S.A.P.I. DE C.V.','S. DE R.L. DE C.V.','S. DE R.L. MI','S.C.','S.C.L.','S.P.A.','S.L.','L.T.D.','LTDA','L.L.C.','S.R.L.','INC.');
        //arreglo pasa tipos de sociedades
        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');

        return $this->render('ClienteClienteBundle:Default:agregar_cliente_user.html.twig', array('tipoSoc' => $tipoSoc, 'paises' => $result_paises ));
    }//mostrar vista de agregar
    public function addCliente_userAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        $idDistribuidor=$perfil['idDistribuidor'];
        //obtener idUsuario
        $request = $this->getRequest();
        if ($request->getMethod() == 'POST')
        {
            $id = $request->get('idCliente');
            $accion = $request->get('accion');
            $nom = $this->limpiar($request->get('inputNom'));
            $rfc = $this->limpiar($request->get('inputRFC'));
            $email = $this->limpiar($request->get('email'));
            $tel = $this->limpiar($request->get('tel'));
            $tel2 = $this->limpiar($request->get('tel2'));
            $calle = $this->limpiar($request->get('calle'));
            $col = $this->limpiar($request->get('col'));
            $pais = $this->limpiar($request->get('DE_id_Pais'));
            $estado = $this->limpiar($request->get('DE_id_Estado'));
            $ciudad = $this->limpiar($request->get('DE_id_Ciudad'));
            $cp = $this->limpiar($request->get('inputCP'));
            $rs = $this->limpiar($request->get('rs'));
            $trs = $this->limpiar($request->get('trs'));
        }
        $data= null;
        $SParr = null;
        $SParr .= "idCliente: " . $id . "; ";
        $SParr .= "CL_Nombre: " . $nom . "; ";
        $SParr .= "CL_RFC: " . $rfc . "; ";
        $SParr .= "CL_TipoSoc: " . $trs . "; ";
        $SParr .= "CL_RazonSocial: " . $rs . "; ";
        $SParr .= "CL_Email: " . $email . "; ";
        $SParr .= "CL_Telefono: " . $tel . "; ";
        $SParr .= "CL_Telefono2: " . $tel2 . "; ";
        $SParr .= "idPais: " . $pais . "; ";
        $SParr .= "idEstado: " . $estado . "; ";
        $SParr .= "idCiudad: " . $ciudad . "; ";
        $SParr .= "Colonia: " . $col . "; ";
        $SParr .= "Calle: " . $calle . "; ";
        $SParr .= "CP: " . $cp . "; ";
        $SParr .= "idUsuario: " . $idUsuario . "; ";
        $SParr .= "idDistribuidor: " . $idDistribuidor . "; ";
        $SParr .= "accion: " . $accion . "; "; //1=insertar 2=update (DIRECCIONES)
        $SParr = str_replace('"', '\"', $SParr);

        $data = $this->FM->Run_Script('PHP_DIST', 'PHP_CTE_Insert_Update', $SParr);
        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
        }    
        return $this->redirect($this->generateUrl('cliente_user'));
    }//agregar cliente
    public function detalle_cliente_userAction($idCliente)
    {
        $request = $this->getRequest();
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idCliente');
        }
        $cte="select * from CTE where idCliente=$id ";
        $this->ODBC->setQuery($cte);
        $this->ODBC->exec();
        $cliente = $this->ODBC->getResultAssoc();

        $query="select * from DIRCTE where idCliente=$id ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $direcciones = $this->ODBC->getResultAssoc();

        $query2="select b.idRepresentante,b.RE_NombreCompleto,b.RE_RFC from CTERP a inner join REP b on(a.idRepresentante=b.idRepresentante) where a.idCliente=$id ";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $representes = $this->ODBC->getResultAssoc();

        $query3="select DISTINCT idRepresentante,RE_NombreCompleto,RE_RFC from REP order by RE_NombreCompleto ";
        $this->ODBC->setQuery($query3);
        $this->ODBC->exec();
        $representesTabla = $this->ODBC->getResultAssoc();

        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');

        return $this->render('ClienteClienteBundle:Default:detalle_cliente_user.html.twig', array('cliente' => $id, 'direcciones' => $direcciones, 'representantes' => $representes, 'paises' => $result_paises, 'representesTabla' => $representesTabla, 'DatosCliente' => $cliente ));
    }//detalle cliente

    public function agregar_direccion_cliente_userAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        //obtener idUsuario
        $user = $this->getUser();
        $perfil = $user->getData();
        $idUsuario=$perfil['idUsuario'];
        //obtener idUsuario
        if ($request->getMethod() == 'POST')
        {
            $id = $request->get('idCliente');
            $accion = $request->get('accion');
            $idDireccionCliente = $request->get('idDireccionCliente');
            $pais = $this->limpiar($request->get('DE_id_Pais'));
            $estado = $this->limpiar($request->get('DE_id_Estado'));
            $ciudad = $this->limpiar($request->get('DE_id_Ciudad'));
            $cp = $this->limpiar($request->get('inputCP'));
            $col = $this->limpiar($request->get('col'));
            $calle = $this->limpiar($request->get('calle'));
        }
        $data= null;
        $SParr = null;
        $SParr .= "idCliente: " . $id . "; ";
        $SParr .= "idPais: " . $pais . "; ";
        $SParr .= "idEstado: " . $estado . "; ";
        $SParr .= "idCiudad: " . $ciudad . "; ";
        $SParr .= "Colonia: " . $col . "; ";
        $SParr .= "Calle: " . $calle . "; ";
        $SParr .= "CP: " . $cp . "; ";
        $SParr .= "idUsuario: " . $idUsuario . "; ";
        $SParr .= "idDireccionCliente: " . $idDireccionCliente . "; ";
        $SParr .= "accion: " . $accion . "; "; //1=insertar 2=update (DIRECCIONES)
        $SParr = str_replace('"', '\"', $SParr);

        $data = $this->FM->Run_Script('PHP_DIRCTE', 'PHP_DIRCTE_Insert_Update', $SParr);

        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
        }    
        return $this->redirect($this->generateUrl('detalle_cliente_user',array('idCliente' => $id))); 
    }//agregar direccion a cliente
    public function agregar_representante_cliente_userAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();

        if ($request->getMethod() == 'POST')
        {
            $id = $request->get('idCliente');
            $accion = $request->get('accion');
            $idRepresentante = $request->get('representante');
            $post = $request->request->all();
        }
        $data= null;
        $SParr = null;
        $SParr .= "idCliente: " . $id . "; ";
        $SParr .= "idRepresentante: " . $idRepresentante . "; ";
        $SParr .= "accion: " . $accion . "; "; //1=insertar 2=update (DIRECCIONES)
        $SParr = str_replace('"', '\"', $SParr);

        $data = $this->FM->Run_Script('PHP_DIRCTE', 'PHP_CTERP_Insert', $SParr);

        if($data == null)
        {
            $session->getFlashBag()->add('flash1', 'Surgio un error');
        }
        else
        {
            $session->getFlashBag()->add('flash1', 'Operación realizada con éxito');
        } 
        return $this->redirect($this->generateUrl('detalle_cliente_user',array('idCliente' => $id)));   
    }//agregar representantes a los clientes
    
    public function ver_cliente_representante_userAction($idRepresentante)
    {
        $request = $this->getRequest();
        if ($request->getMethod() == 'GET')
        {
            $idRepresentante = $request->get('idRepresentante');
        }
        $rep="select * from REP where idRepresentante=$idRepresentante ";
        $this->ODBC->setQuery($rep);
        $this->ODBC->exec();
        $representante = $this->ODBC->getResultAssoc();
        $idCliente=$representante['data'][0]['idCliente'];

        $cte="select b.idCliente,b.CL_Nombre,b.CL_RazonSocial,b.CL_RFC from CTERP a inner join CTE b on(a.idCliente=b.idCliente) where a.idRepresentante=$idRepresentante ";
        $this->ODBC->setQuery($cte);
        $this->ODBC->exec();
        $clientes = $this->ODBC->getResultAssoc();

        return $this->render('ClienteClienteBundle:Default:cliente_representante_user.html.twig', array('representante' => $representante, 'clientes' => $clientes ));
    }//ver cliente representante
    //USER//
    public function cliente_viewAction()
    {
        $query="select * from CTE order by idCliente";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        return $this->render('ClienteClienteBundle:Default:cliente_view.html.twig', array('clientes' => $result ));
    }
    public function detalle_cliente_viewAction($idCliente)
    {
        $request = $this->getRequest();
        if ($request->getMethod() == 'GET')
        {
            $id = $request->get('idCliente');
        }
        $cte="select * from CTE where idCliente=$id ";
        $this->ODBC->setQuery($cte);
        $this->ODBC->exec();
        $cliente = $this->ODBC->getResultAssoc();

        $query="select * from DIRCTE where idCliente=$id ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $direcciones = $this->ODBC->getResultAssoc();

        $query2="select b.idRepresentante,b.RE_NombreCompleto,b.RE_RFC from CTERP a inner join REP b on(a.idRepresentante=b.idRepresentante) where a.idCliente=$id ";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $representes = $this->ODBC->getResultAssoc();

        $query3="select DISTINCT idRepresentante,RE_NombreCompleto,RE_RFC from REP order by RE_NombreCompleto ";
        $this->ODBC->setQuery($query3);
        $this->ODBC->exec();
        $representesTabla = $this->ODBC->getResultAssoc();

        $PECC = $this->get('pecc');
        $result_paises = $PECC->getPaises('es');

        return $this->render('ClienteClienteBundle:Default:detalle_cliente_view.html.twig', array('cliente' => $id, 'direcciones' => $direcciones, 'representantes' => $representes, 'paises' => $result_paises, 'representesTabla' => $representesTabla, 'DatosCliente' => $cliente ));
    }//detalle cliente
    public function limpiar($msj)
    {
        $msj = str_replace(
        array("\\", "¨", "º", "-", "~",
        "#", "|", "!", "\"",
        "·", "$", "%", "&", "/",
        "(", ")", "?", "'", "¡",
        "¿", "[", "^", "`", "]",
        "+", "}", "{", "¨", "´",
        ">", "< ", ";", ",", ":",
        "."),
        '',
        $msj
        );
        return $msj;
    }
}