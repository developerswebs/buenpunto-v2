<?php

namespace Reporte\ReporteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Utilerias\FileMakerBundle\API\ODBC\Client; //libreria para conexion con ODBC, para querys
use Utilerias\FileMakerBundle\API\FM11API; //libreria para conexion a FM, para insert, delete, update

class DefaultController extends Controller
{
	private $ODBC = null;
    private $FM = null;

    public function __construct() { //constructor para usar usar al objeto FM y poder utilizar las clases de la libreria ODBC
        $this->ODBC = new Client();
        $this->FM = new FM11API();
    } //CONSTRUCTORES PARA LIBRERIAS

    public function reporteAction()
    {
        return $this->render('ReporteReporteBundle:Default:reporte.html.twig');
    }
    public function rpt1Action()
    {
        $data = $this->FM->Do_Query("PHP_RPT_CTE_ACTIVIDAD");
        return $this->render('ReporteReporteBundle:Default:rpt1.html.twig',array('usuarios' => $data));
    }//Reporte Actividad de los Usuarios
    public function rpt2Action()
    {
        $data = $this->FM->Do_Query("PHP_REPORTE_SALDO_CTE",array("cte_RPT::RP_D_I" => 0),array("CL_Nombre" => 'ascend'));
        return $this->render('ReporteReporteBundle:Default:rpt2.html.twig',array('clientes' => $data));
    }//Reporte de Saldo por Clientes
    public function rpt3Action()
    {
        $data = $this->FM->Do_Query("PHP_REPORTE_MOV_GRAL","",array("rpt_CTE::CL_RazonSocial" => 'ascend', "c_RP_Numero" => 'ascend', "RP_FechaCreacion" => 'ascend'));
        $temp = array();
        foreach ($data as $value) {
        if(!is_array($temp[$value['idCliente']])){
        $temp[$value['idCliente']] = array();
        }
        array_push($temp[$value['idCliente']],$value);
        }
        return $this->render('ReporteReporteBundle:Default:rpt3.html.twig',array('clientes' => $temp));
    }//Reporte de Movimientos por Clientes General
    public function rpt4Action()
    {
        $data = $this->FM->Do_Query("PHP_RPT_MAYOR500k","",array("cte_RPT::RP_Balance_ID" => 'des'));
        //print_r($data);
        //die();
        return $this->render('ReporteReporteBundle:Default:rpt4.html.twig',array('clientes' => $data));
    }//Reporte monto mayor a $500,000.00
    public function rpt5Action()
    {   /*
        $query="select DISTINCT a.idCliente,a.CL_RazonSocial,a.CL_NumeroMovimientosXCliente,a.CL_NumeroDistribuidoresReportaron,b.RP_Balance_ID from CTE a inner join RPT b on(a.idCliente=b.idCliente) order by a.CL_NumeroDistribuidoresReportaron DESC ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        $result2=array();
        for($i=0,$limit=10;$i<$limit;$i++)
        {
            $result2[$i]=$result['data'][$i];
        }*/
        $Args['cte_RPT::RP_Balance_ID']='>0';
        $data = $this->FM->Do_Query("PHP_RPT_CTE_10MAS",$Args,array("CL_NumeroDistribuidoresReportaron" => 'des'));
        $result2=array();
        for($i=0,$limit=10;$i<$limit;$i++)
        {
            $result2[$i]=$data[$i];
        }
        //print_r($result2);
        //die();
        return $this->render('ReporteReporteBundle:Default:rpt5.html.twig',array('clientes' => $result2));
    }//Reporte los 10 más reportados
    public function rpt6Action()
    {   //WHERE fecha BETWEEN '20121201' AND '20121202' //rangos de fechas
        $date1=date("01/01/2005"); //principios de los tiempos
        $date2=date("m/d/Y"); //fecha actual
        $dateD=date("m/*/Y"); //fecha FM
        $date22=date("m/01/Y"); //fecha actual primero de mes
        $date3=date('m/d/Y', strtotime('now - 1 month')); //fecha mes anterior
        $dateD2=date('m/*/Y', strtotime('now - 1 month')); ///fecha FM2
        $date31=date('m/31/Y', strtotime('now - 1 month')); //fecha mes anterior
        $date33=date('m/01/Y', strtotime('now - 1 month')); //fecha mes anterior primero de mes

        $query="select COUNT(idCliente) from CTE where CL_FechaCreacion BETWEEN '$date1' and '$date2' ";
        $this->ODBC->setQuery($query);
        $this->ODBC->exec();
        $result = $this->ODBC->getResultAssoc();
        $a=$result['data'][0];
        $query2="select COUNT(idCliente) from CTE where CL_FechaCreacion BETWEEN '$date1' and '$date3' ";
        $this->ODBC->setQuery($query2);
        $this->ODBC->exec();
        $result2 = $this->ODBC->getResultAssoc();
        $b=$result2['data'][0];
        
        $query3="select COUNT(idCliente) from CTE where CL_FechaCreacion BETWEEN '$date22' and '$date2' ";
        $this->ODBC->setQuery($query3);
        $this->ODBC->exec();
        $result3 = $this->ODBC->getResultAssoc();
        $c=$result3['data'][0];
        $query4="select COUNT(idCliente) from CTE where CL_FechaCreacion BETWEEN '$date33' and '$date31' ";
        $this->ODBC->setQuery($query4);
        $this->ODBC->exec();
        $result4 = $this->ODBC->getResultAssoc();
        $d=$result4['data'][0];
        //Primer paquete de campos//
        $Args['RP_FechaCreacion']='<='.$date2;
        $data = $this->FM->Do_Query("PHP_RPT_ACUMULADO_MES",$Args);

        $Cargo=$data[0]['RptCargoAcumulado'];
        $Abono=$data[0]['RptAbonoAcumulado'];

        $Args2['RP_FechaCreacion']='<='.$date31;
        $data2 = $this->FM->Do_Query("PHP_RPT_ACUMULADO_MES",$Args2);

        $Cargo2=$data2[0]['RptCargoAcumulado'];
        $Abono2=$data2[0]['RptAbonoAcumulado'];
        //Segundo paquete de campos//
        $Args3['RP_FechaCreacion']='=='.$dateD;
        $data3 = $this->FM->Do_Query("PHP_RPT_ACUMULADO_MES",$Args3);

        $Cargo3=$data3[0]['RptCargoAcumulado'];
        $Abono3=$data3[0]['RptAbonoAcumulado'];
        //Tercer paquete de campos//
        $Args4['RP_FechaCreacion']='=='.$dateD2;
        $data4 = $this->FM->Do_Query("PHP_RPT_ACUMULADO_MES",$Args4);

        $Cargo4=$data4[0]['RptCargoAcumulado'];
        $Abono4=$data4[0]['RptAbonoAcumulado'];

        return $this->render('ReporteReporteBundle:Default:rpt6.html.twig',array('a' => $a, 'b' => $b, 'c' => $c, 'd' => $d,'C1' => $Cargo, 'A1' => $Abono, 'C2' => $Cargo2, 'A2' => $Abono2, 'C3' => $Cargo3, 'A3' => $Abono3, 'C4' => $Cargo4, 'A4' => $Abono4));
    }//Reporte del mes anterior
    public function rpt7Action()
    {   //array("rpt_CTE::CL_RazonSocial" => 'ascend', "c_RP_Numero" => 'ascend', "RP_FechaCreacion" => 'ascend'
        //filtro array("RP_D_I" => 1)
        $data = $this->FM->Do_Query("PHP_RPT_ID",array("RP_D_I" => 1),array("rpt_CTE::CL_RazonSocial" => 'ascend', "c_RP_Numero" => 'ascend', "RP_FechaCreacion" => 'ascend'));
        $temp = array();
        foreach ($data as $value) {
            if(!is_array($temp[$value['idCliente']])){
            $temp[$value['idCliente']] = array();
            }
            array_push($temp[$value['idCliente']],$value);
        }
        //print_r($data);
        //die();
        return $this->render('ReporteReporteBundle:Default:rpt7.html.twig',array('clientes' => $temp));
    }//Reporte de Desaparecidos e Incobrables

}//cierre clase