$('#DE_id_Pais').change(find_estados);
$('#DE_id_Estado').change(find_ciudades);
$('#DE_id_Ciudad').change(find_colonias);

function find_estados() {
    var id_Pais = $(this).val();
    var url = $('#PECC_Estado').val() + '/../' + id_Pais;
    $('#processing-modal').modal('show');
    $('#loading_msj').html('Cargando Estados...'); 
    $.ajax({
        type: "post",
        dataType: 'json',
        url: url,
        success: function(data, text) {
            //console.log(data);
            /*if(text == 'success'){
                console.log('ENCONTRE ESTADOS');
            }else{
                console.log('NO ENCONTRE ESTADOS');
            }*/
            if (!data['status']) {
                
                show_error_modal(data['data']);
                return;
            }

            $('#DE_Pais').val($('#DE_id_Pais option:selected').text());

            var option = '';

            if (data['data'] !== null) {
                    option += '<option value="" disabled selected>Selecciona tu estado...</option>';
                $.each(data['data'], function(id_estado, estado) {
                    option += '<option value="' + id_estado + '" >' + estado + '</option>';
                });
            }
            $('#processing-modal').modal('hide');  
            $('#DE_Estado').val('');
            $('#DE_id_Estado').html(option);
            $('#DE_id_Estado').selectpicker('refresh');
        },
        error: function(request, status, error) {
            //console.log('NO ENCONTRE ESTADOS');
            show_error_modal(request.responseText);
        }
    });
}
function find_ciudades() {
    var id_Estado = $(this).val();
    var url = $('#PECC_Ciudad').val() + '/../' + id_Estado;
    $('#processing-modal').modal('show');
    $('#loading_msj').html('Cargando Ciudades...');

    $.ajax({
        type: "post",
        dataType: 'json',
        url: url,
        success: function(data, text) {

            if (!data['status']) {
                show_error_modal(data['data']);
                return;
            }

            $('#DE_Estado').val($('#DE_id_Estado option:selected').text());

            var option = '';

            if (data['data'] !== null) {
                    option += '<option value="" disabled selected>Selecciona tu ciudad...</option>';
                $.each(data['data'], function(id_ciudad, ciudad) {
                    option += '<option value="' + id_ciudad + '" >' + ciudad + '</option>';
                });
            }
            $('#processing-modal').modal('hide'); 
            $('#DE_Ciudad').val('');
            $('#DE_id_Ciudad').html(option);
            $('#DE_id_Ciudad').selectpicker('refresh');

        },
        error: function(request, status, error) {
            $('#loader_ciudad').fadeOut();
            show_error_modal(request.responseText);
        }
    });
}
function find_colonias() {
    var id_Pais = $(this).val();
    var url = $('#PECC_Colonia').val() + '/../' + id_Pais;

    $.ajax({
        type: "post",
        dataType: 'json',
        url: url,
        success: function(data, text) {

            if (!data['status']) {
                show_error_modal(data['data']);
                return;
            }

            $('#DE_Pais').val($('#DE_id_Pais option:selected').text());

            var option = '';

            if (data['data'] !== null) {
                $.each(data['data'], function(id_colonia, colonia) {
                    option += '<option value="' + id_colonia + '" >' + colonia + '</option>';
                });
            }

            $('#DE_Colonia').val('');
            $('#DE_id_Colonia').html(option);
            $('#DE_id_Colonia').selectpicker('refresh');
        },
        error: function(request, status, error) {
            $('#loader_colonia').fadeOut();
            show_error_modal(request.responseText);
        }
    });
}
function change_estado() {
    $('#DE_Estado').val($('#DE_id_Estado option:selected').text());
    reset_address();
}

function reset_address() {
    $('#DE_CP').val('');
    $('#DE_Ciudad').val('');
    $('#DE_id_Ciudad').val('');
    $('#DE_Colonia').val('');
    $('#DE_id_Colonia').val('');
    $('#DE_Calle').val('');
    $('#DE_NumeroExterior').val('');
    $('#DE_Interior').val('');
}

function hide_colonia() {
    $('#DE_Colonia').removeClass('required');
    $('#DE_Colonia').parent().find('img.img_required').remove();
    $('#DE_CP').removeClass('required');
    $('#DE_CP').parent().find('img.img_required').remove();
    /*$('#DE_Colonia').parent().fadeOut();
     $('#DE_Colonia').attr('disabled', 'disabled');
     $('#DE_id_Colonia').attr('disabled', 'disabled');*/
}

function show_colonia() {
    var img = jQuery('<img/>', {
        "class": "img_required",
        "src": $('#img_required').val(),
        "alt": 'required'
           });

    $('#DE_CP').after(img);
    $('#DE_CP').addClass('required');

    var img = jQuery('<img/>', {
        "class": "img_required",
        "src": $('#img_required').val(),
        "alt": 'required'
           });

    $('#DE_Colonia').after(img);
    $('#DE_Colonia').addClass('required');
    /*$('#DE_Colonia').removeAttr('disabled');
     $('#DE_id_Colonia').removeAttr('disabled');
     $('#DE_Colonia').parent().fadeIn();*/
}