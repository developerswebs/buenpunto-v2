//JQUERY PARA DESABILITAR F5
function disableF5(e) {
    if ((e.which || e.keyCode) == 116)
        e.preventDefault();
};
/* jQuery < 1.7 */
$(document).bind("keydown", disableF5);
/* OR jQuery >= 1.7 */
$(document).on("keydown", disableF5);